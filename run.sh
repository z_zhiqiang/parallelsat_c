#!/usr/bin/env bash
# create build folder and generate make files
mkdir -p build
cd build
cmake ..

# build the code
make

# run the code
ctest -VV
./sequentialsolver $@

# return back the current directory
cd ..
