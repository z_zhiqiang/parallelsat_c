#include "Timer.hpp"


/* Class Constructors & Destructor */
Timer::Timer() 
    : startTime{std::clock()}, title{"Untitle"} {

}


Timer::Timer(const std::string& title) 
    : startTime{std::clock()}, title{title} {

}


Timer::~Timer() {

}


/* Public Methods */
std::string Timer::result() {
    int mili = std::clock() - startTime;
    double sec = mili / 1000.0;
    double minute = sec / 60.0;
    double hour = minute / 60.0;
    return std::string{"Time for " + title + " = " +
        to_string_with_precision(mili, 3) + " miliseconds = " + 
        to_string_with_precision(sec, 3) + " seconds = " +
        to_string_with_precision(minute, 3) + " minutes = " +
        to_string_with_precision(hour, 3) + " hours"};
}


/* Private Methods */
template <typename T>
std::string Timer::to_string_with_precision(const T a_value, const int& n) {
    std::ostringstream out;
    out << std::setprecision(n) << a_value;
    return out.str();
}
