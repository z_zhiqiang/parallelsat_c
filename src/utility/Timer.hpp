#ifndef TIMER_HPP
#define TIMER_HPP

#include <ctime>
#include <string>
#include <sstream>
#include <iomanip>


// This class represents a timer to calculate the time elapsed
class Timer {
    
    public:
        /* Class Constructors & Destructor */
        Timer();
        Timer(const std::string& title);
        virtual ~Timer();


        /* Public Methods */
        std::string result();


    private:
        /* Declaring Variables */
        std::clock_t startTime;
        std::string title;


        /* Private Methods */
        template <typename T>
        std::string to_string_with_precision(const T a_value, const int& n);
};

#endif // TIMER_HPP
