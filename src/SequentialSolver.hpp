#ifndef SEQUENTIALSOLVER_HPP
#define SEQUENTIALSOLVER_HPP

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <algorithm>

#include "utility/Timer.hpp"
#include "trieimpl/Bucket.hpp"
#include "trieimpl/LiteralCounts.hpp"


// This class represents the parallel solver
class SequentialSolver {

    public:
        /* Class Constructors & Destructor */
        SequentialSolver();
        SequentialSolver(const std::string& input);
        virtual ~SequentialSolver();

        /* Public Methods */
        // This method will run the solver
        void run();


    private:
        /* Declaring Variables */
        std::string inputFile;
        std::unique_ptr<int[]> staticOrdering;
        Bucket** buckets;
        LiteralCounts literalCounts;
        int numOfVariables;

        /* Private Methods */
        // This method will compute the static orderings of the current input file
        // and return the mapping (ascending order)
        void computeStaticOrdering();

        // This method will preprocess the file with the static ordering and return
        // the buckets
        void preprocess(); 

        // This method will print the curernt buckets (for debugging)
        void printBuckets();

        // This method will do the resolution with the given bucket and update the
        // literal counts + buckets
        bool doResolution(Bucket* bucket);

        // This method will do the resolution recursively by getting each clause
        // out one by one
        void doResolutionRecursive(const TrieClauses::Node* node, std::vector<Literal>& literals, const TrieClauses& resolutionWith); 
};

#endif // SOLVER_HPP
