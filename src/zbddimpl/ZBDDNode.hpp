#include <ostream>

class ZBDDNode{

	//toString
	friend std::ostream& operator<<(std::ostream& os, const ZBDDNode& node);

public:
	//constructor & destructor
	ZBDDNode(unsigned v, ZBDDNode* ln, ZBDDNode* hn);
	virtual ~ZBDDNode();


	/*getters*/
	inline unsigned getValue() const {
		return value;
	}

	inline ZBDDNode* getLow() const {
		return low;
	}

	inline ZBDDNode* getHigh() const {
		return high;
	}

	/*determine whether a node is 1-terminal node*/
	inline bool isONE() const {
		return value == -1;
	}

	/*determine whether a node is 0-terminal node*/
	inline bool isZERO() const {
		return value == 0;
	}


	/*recursive function: get the total number of paths under this node*/
	const long count() const;


private:
	unsigned value;
	ZBDDNode * low;
	ZBDDNode * high;
};


class TripleID{

	friend std::ostream& operator<<(std::ostream& os, const TripleID& triple);

public:
	TripleID(int val, long lid, long hid);
	virtual ~TripleID();

	//operator for map
	bool operator<(const TripleID& other) const {
		if(value == other.value){
			if(low_id == other.low_id){
				return high_id < other.high_id;
			}
			return low_id < other.low_id;
		}
		return value < other.value;
	}

private:
	unsigned value;
	long low_id;
	long high_id;

};

