/*
 * ZBDDNode.cpp
 *
 *  Created on: Nov 15, 2016
 *      Author: icuzzq
 */
#include <ostream>
#include "ZBDDNode.hpp"

ZBDDNode::ZBDDNode(unsigned val, ZBDDNode * ln, ZBDDNode * hn): value(val), low(ln), high(hn) {}

ZBDDNode::~ZBDDNode(){}


const long ZBDDNode::count() const {
	if(this->isONE()){
		return 1;
	}

	if(this->isZERO()){
		return 0;
	}

	return this->low->count() + this->high->count();
}


std::ostream& operator<<(std::ostream& os, const ZBDDNode& node){
	//TODO

}


/*
 * for class TripleID
 */

TripleID::TripleID(int val, long lid, long hid): value(val), low_id(lid), high_id(hid) {}

TripleID::~TripleID(){}

std::ostream& operator<<(std::ostream& os, const TripleID& triple){
	//TODO

}
