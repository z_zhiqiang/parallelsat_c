#ifndef ABSTRACTDATASTRUCTURE_HPP
#define ABSTRACTDATASTRUCTURE_HPP

#include <map>
#include "Clause.hpp"


class AbstractDataStructure {

    public:
        /* Class Constructors & Destructor */
        virtual ~AbstractDataStructure() = 0;

        /* Class Operators */
        friend std::ostream& operator<<(std::ostream& strm, const AbstractDataStructure& data);

        /* Getters & Setters */
        virtual const int& getKey() const = 0;
        virtual unsigned getSize() const = 0;

        /* Public Methods */
        // This method will add a clause to the data structure
        virtual void add(const Clause& clause) = 0;

        // This method will add another data structure to this current data structure
        virtual void add(const AbstractDataStructure& data) = 0;

        // This method will retrieve all the clauses with given specific literal
        // in this data structure and remove these clauses from the data structure
        virtual AbstractDataStructure* retrieve(const Literal& literal) = 0;

        // This method will do the resolution with a clause from this structure
        virtual std::map<int,AbstractDataStructure> doResolution(const Clause& clause) const = 0;

        // This method will remove everything from this data structure
        virtual void reset() = 0;

};

#endif //ABSTRACTDATASTRUCTURE_HPP
