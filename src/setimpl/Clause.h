/*
 * Clause.h
 *
 *  Created on: Aug 11, 2016
 *      Author: icuzzq
 */

#ifndef CLAUSE_H_
#define CLAUSE_H_

#include <unordered_set>
#include <string>
#include <vector>

//using namespace std;

class Clause {

	friend std::ostream& operator<<(std::ostream& os, const Clause& clause);

	public:
		Clause();
		Clause(std::string& line, std::vector<int>& orders);
		Clause(Clause c1, Clause c2, int key, std::vector<int>& orders);
		virtual ~Clause();

		bool operator==(const Clause & other) const {
			if (literals.size() != other.literals.size()
					|| indicator != other.indicator || key != other.key) {
				return false;
			}

			for (auto it = other.literals.begin(); it != other.literals.end();
					++it) {
				if (literals.find(*it) == literals.end()) {
					return false;
				}
			}

			return true;
//			return false;
		}

//		size_t hash_value(){
//			size_t value = 0;
//			for (auto it = this->literals.begin(); it != this->literals.end(); ++it) {
//				value += 13 * *it;
//			}
//			return value;
//		}


		bool isTrue() {
			return this->indicator == 1;
		}

		bool isFalse(){
			return this->indicator == -1;
		}

		bool isPositive(){
			return this->indicator == 2;
		}

		bool isNegative(){
			return this->indicator == -2;
		}

		bool isInitial(){
			return this->indicator == 0;
		}

		int getKey(){
			return key;
		}

//		std::unordered_set<int> getLiterals() {
//			return literals;
//		}
		std::unordered_set<int> literals;

	private:
		int key;
		char indicator = 0;


		int generateKey(std::vector<int>& orders){
			for(auto it = orders.begin(); it != orders.end(); ++it){
				int var = *it;
				if(this->literals.find(var) != this->literals.end()){
					this->indicator = 2;
					return var;
				}

				if(this->literals.find(0 - var) != this->literals.end()){
					this->indicator = -2;
					return var;
				}
			}

			return 0;
		}

};

#endif /* CLAUSE_H_ */
