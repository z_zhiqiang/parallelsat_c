/*
 * hash_Clause.h
 *
 *  Created on: Aug 19, 2016
 *      Author: icuzzq
 */

#ifndef HASH_CLAUSE_H_
#define HASH_CLAUSE_H_

#include <unordered_set>

#include "Clause.h"

namespace std
{
	template <>
	struct hash<Clause>
	{
		size_t operator()(const Clause&  clause) const
		{
			size_t value = 0;
			for (auto it = clause.literals.begin(); it != clause.literals.end(); ++it) {
				value += *it;
			}
			return value;
		}
	};

}


#endif /* HASH_CLAUSE_H_ */
