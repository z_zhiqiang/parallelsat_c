/*
 * uci_psat_util.h
 *
 *  Created on: Aug 19, 2016
 *      Author: icuzzq
 */

#ifndef UCI_PSAT_UTIL_H_
#define UCI_PSAT_UTIL_H_

#include <iostream>
#include <string>
#include <unordered_set>

#include "Clause.h"
#include "hash_Clause.h"

using namespace std;

void print_set(std::unordered_set<Clause> & set){
	cout << set.size() << endl;
	cout << "-----------------" << endl;
	for (auto it = set.begin(); it != set.end(); ++it) {
		cout << *it << endl;

		//delete memory
//		delete *it;
	}
	cout << "-----------------\n\n" << endl;

}

void delete_set(std::unordered_set<Clause> & set){
//	for (auto it = set.begin(); it != set.end(); ++it) {
////		cout << *it << endl;
//
//		//delete memory
//
//		Clause clause = *it;
//		clause.~Clause();
//	}
	set.clear();
}

#endif /* UCI_PSAT_UTIL_H_ */
