//============================================================================
// Name        : ParallelSAT.cpp
// Author      : Zhiqiang Zuo
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include <fstream>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/lexical_cast.hpp>

#include <array>
#include <unordered_set>
#include <vector>

#include "Clause.h"
#include "hash_Clause.h"
#include "uci_psat_util.h"

//#include "omp.h"

using namespace std;
using namespace boost;



int readOrdering(std::string&, vector<int>&);
void readCNF(string& file_input, vector<std::unordered_set<Clause>>& positive, vector<std::unordered_set<Clause>>& negative, vector<int>& orders_vector);
void doOrderedResolution(vector<std::unordered_set<Clause>>& positive, vector<std::unordered_set<Clause>>& negative, vector<int>& orders_vector);

int BUCKET_NUM = 100;

int main() {
	std::string cnf_file("/home/icuzzq/Workspace/git/parallelsat_c/testcases/factoring1.dimacs");
	std::string order_file("/home/icuzzq/Workspace/git/parallelsat_c/testcases/orders.txt");

	vector<int> orders;
	int Bucket_Num = readOrdering(order_file, orders);

	//for debugging
	for(auto it = orders.begin(); it != orders.end(); ++it){
		cout << *it << "|";
	}
	cout << endl;


	//buckets
//	std::unordered_set<Clause> positive [Bucket_Num];
//	std::unordered_set<Clause> negative [Bucket_Num];

//	for(int i = 0; i < Bucket_Num; i++){
//		positive[i] = std::unordered_set<Clause>();
//		negative[i] = std::unordered_set<Clause>();
//	}

	vector<unordered_set<Clause>> positive;
	vector<unordered_set<Clause>> negative;
	positive.reserve(Bucket_Num);
	negative.reserve(Bucket_Num);
	for(int i = 0; i < Bucket_Num; i++){
		positive.push_back(std::unordered_set<Clause>());
		negative.push_back(std::unordered_set<Clause>());
	}


	//read cnf from input file
	readCNF(cnf_file, positive, negative, orders);

	//for debugging and memory cleaning
	int count = 0;
	for(unsigned i = 0; i < orders.size(); i++){
		int key = orders[i];
		cout << "bucket " << key << endl;

		std::unordered_set<Clause> pset = positive[key - 1];
		count += pset.size();
		print_set(pset);

		std::unordered_set<Clause> nset = negative[key - 1];
		count += nset.size();
		print_set(nset);
	}
	cout << "#clauses: " << count << endl;


	doOrderedResolution(positive, negative, orders);

}

void doOrderedResolution(vector<std::unordered_set<Clause>>& positive, vector<std::unordered_set<Clause>>& negative, vector<int>& orders_vector){
	for(auto it = orders_vector.begin(); it != orders_vector.end(); ++it){
		int bucket_key = *it;
		cout << "For bucket: " << bucket_key << endl;

		std::unordered_set<Clause> positive_clauses = positive[bucket_key - 1];
		std::unordered_set<Clause> negative_clauses = negative[bucket_key - 1];

		if(positive_clauses.empty() || negative_clauses.empty()){
			continue;
		}

		//do resolution for one bucket
//		#pragma omp parallel
		{
			for (auto it_pos = positive_clauses.begin(); it_pos != positive_clauses.end(); ++it_pos) {
				Clause pos_clause = *it_pos;
//				#pragma omp for schedule(dynamic)
				for (auto it_neg = negative_clauses.begin(); it_neg != negative_clauses.end(); ++it_neg) {
					Clause neg_clause = *it_neg;

					Clause* new_clause = new Clause(pos_clause, neg_clause, bucket_key, orders_vector);
//					cout << pos_clause << endl;
//					cout << neg_clause << endl;
					cout << *new_clause << endl;
//					cout << "================================" << endl;

					if (new_clause->isFalse()) {
						cout << *new_clause << endl;
						cout << "UNSAT!!!" << endl;
						return;
					} else if (new_clause->isTrue()) {
						//skip the true clause
//						new_clause.getLiterals().clear();
					} else if (new_clause->isPositive()) {
//						print_set(positive[new_clause.getKey() - 1]);
						positive[new_clause->getKey() - 1].insert(*new_clause);
//						print_set(positive[new_clause.getKey() - 1]);
					} else if (new_clause->isNegative()){
						negative[new_clause->getKey() - 1].insert(*new_clause);
					}
				}
			}

		}


		//free bucket_key
		delete_set(positive_clauses);
		delete_set(negative_clauses);


	}
	cout << "SAT!!!" << endl;

}

int readOrdering(string& order_file, vector<int>& orders_vector){
	int number_buckets = 0;

	std::ifstream infile(order_file);
	std::string line;
	while(std::getline(infile, line)){
		number_buckets = stoi(line);
		break;
	}

	for(int i = 1; i <= number_buckets; i++){
		orders_vector.push_back(i);
	}

	return number_buckets;
}

void readCNF(string& file_input, vector<std::unordered_set<Clause>>& positive, vector<std::unordered_set<Clause>>& negative, vector<int>& orders_vector){
	std::ifstream infile(file_input);
	std::string line;

	while(std::getline(infile, line)){
		if(boost::starts_with(line, "c")){

		}
		else if(boost::starts_with(line, "p")){
			char_separator<char> sep(" ");
			tokenizer<char_separator<char>> tokens(line, sep);

			tokenizer<char_separator<char>>::iterator it = tokens.begin();
			std::advance(it, 2);
			int number_vars = boost::lexical_cast<int>(*it);
			it++;
			int number_clauses = boost::lexical_cast<int>(*it);

			cout << "Number of variables: " << number_vars << endl;
			cout << "Number of clauses: " << number_clauses << endl;

		}
		else{

			Clause* clause = new Clause(line, orders_vector);
//			cout << clause << endl;

            if(clause->isPositive()){
                positive[clause->getKey() - 1].insert(*clause);

            }
            else{
                negative[clause->getKey() - 1].insert(*clause);
            }

		}
	}



}






