/*
 * Clause.cpp
 *
 *  Created on: Aug 11, 2016
 *      Author: icuzzq
 */
#include "Clause.h"

#include <iostream>
#include <boost/tokenizer.hpp>


using namespace boost;

Clause::Clause() {
	// TODO Auto-generated constructor stub
}

Clause::Clause(std::string& line, std::vector<int>& orders){
	char_separator<char> sep(" ");
	tokenizer<char_separator<char>> tokens(line, sep);


	for (tokenizer<char_separator<char>>::iterator tok_it = tokens.begin(); tok_it != tokens.end(); ++tok_it) {
		std::string t = *tok_it;
		int literal = stoi(t);
		if(literal != 0){
			this->literals.insert(literal);

			//check true clause
			if (this->literals.find(0 - literal) != this->literals.end()) {
				this->indicator = 1;
			}

		}
	}

//	for (const auto & t: tokens) {
//		int literal = stoi(t);
//		this->literals.insert(literal);
//
//		//check true clause
//		if(this->literals.find(0 - literal) != this->literals.end()){
//			this->indicator = 1;
//		}
//	}

    if(isTrue()){
        this->key = 0;
    }
    else{
        this->key = generateKey(orders);
    }

}

void printout_literals(std::unordered_set<int>& literals){
	for(auto it = literals.begin(); it != literals.end(); ++it){
		std::cout << *it << "|";
	}
	std::cout << std::endl;
}

Clause::Clause(Clause c1, Clause c2, int key, std::vector<int>& orders){
	for(auto it = c1.literals.begin(); it != c1.literals.end(); ++it){
		this->literals.insert(*it);
	}
	this->literals.erase(c1.isPositive() ? key : 0 - key);
//	printout_literals(this->literals);


	for(auto it = c2.literals.begin(); it != c2.literals.end(); ++it){
		int literal = *it;
		if(this->literals.find(0 - literal) != this->literals.end()){
			this->indicator = 1;
		}
		this->literals.insert(literal);
	}
	this->literals.erase(c2.isPositive() ? key : 0 - key);


	if(this->isTrue()){
		this->key = 0;
	}
	else if(this->literals.empty()){
		this->indicator = -1;
		this->key = 0;
	}
	else{
		this->key = generateKey(orders);
	}


}



Clause::~Clause() {
	// TODO Auto-generated destructor stub
}



std::ostream& operator<<(std::ostream& os, const Clause& clause){
	os << clause.key << std::endl << (int)clause.indicator << std::endl;
	for(auto it = clause.literals.begin(); it != clause.literals.end(); ++it){
		os << *it << "|";
	}
	os << std::endl;
	return os << std::endl;
}




