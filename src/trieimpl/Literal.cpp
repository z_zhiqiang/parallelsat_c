#include "Literal.hpp"


/* Class Constructors & Destructor */
Literal::Literal() {

}


Literal::Literal(const int& literal) 
    : literal{literal} {

}


Literal::Literal(const Literal& literal) 
    : literal{literal.literal} {

}


Literal::~Literal() {

}


/* Class Operators */
Literal& Literal::operator=(const Literal& literal) {
    if (this == &literal) {
        return *this;
    }

    // copy the literal
    this->literal = literal.literal;
    return *this;
}


bool operator<(const Literal& literal1, const Literal& literal2) {
    int abs1 = std::abs(literal1.literal);
    int abs2 = std::abs(literal2.literal);
    return (abs1 == abs2) ? (literal1.literal < literal2.literal) : (abs1 < abs2);
}


bool operator>(const Literal& literal1, const Literal& literal2) {
    return literal2 < literal1;
}
