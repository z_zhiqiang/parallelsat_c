#ifndef LITERAL_HPP
#define LITERAL_HPP

#include <ostream>
#include <cmath>


// This class is an abstraction for a literal in a clause.
class Literal {

    public:
        /* Class Constructors & Destructor */
        Literal();
        Literal(const int& literal);
        Literal(const Literal& literal);
        virtual ~Literal();


        /* Class Operators */
        friend std::ostream& operator<<(std::ostream& strm, const Literal& literal);
        Literal& operator=(const Literal& literal);

        // sorting style: -1, 1, -2, 2, -3, 3, etc
        friend bool operator==(const Literal& literal1, const Literal& literal2);
        friend bool operator!=(const Literal& literal1, const Literal& literal2);
        friend bool operator<(const Literal& literal1, const Literal& literal2);
        friend bool operator>(const Literal& literal1, const Literal& literal2);


        /* Getters & Setters */
        const int& getLiteral() const;


        /* Public Methods */
        // This method will check whether these literals are opposite to each other or not
        static bool isOpposite(const Literal& literal1, const Literal& literal2);


    private:
        /* Declare Variables */
        int literal;
};


/* Inline Functions */
inline std::ostream& operator<<(std::ostream& strm, const Literal& literal) {
    return strm << literal.literal;
}


inline bool operator==(const Literal& literal1, const Literal& literal2) {
    return literal1.literal == literal2.literal;
}


inline bool operator!=(const Literal& literal1, const Literal& literal2) {
    return literal1.literal != literal2.literal;
}


/* Getters & Setters */
inline const int& Literal::getLiteral() const {
    return literal;
}


/* Public Methods */
// This method will check whether these literals are opposite to each other or not
inline bool Literal::isOpposite(const Literal& literal1, const Literal& literal2) {
    return literal1.literal == -literal2.literal;    
}

#endif // LITERAL_HPP
