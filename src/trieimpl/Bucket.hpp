#ifndef BUCKET_HPP
#define BUCKET_HPP

#include "TrieClauses.hpp"


// This class represents a bucket with clauses
class Bucket {

    public:
        /* Class Constructors & Destructor */
        Bucket();
        Bucket(const int& key);
        Bucket(const Literal& literal);
        Bucket(const Bucket& bucket);
        virtual ~Bucket();


        /* Class Operators */
        friend std::ostream& operator<<(std::ostream& strm, const Bucket& bucket);
        Bucket& operator=(const Bucket& bucket);


        /* Getters & Setters */
        unsigned getSize() const;
        const int& getKey() const;
        const TrieClauses& getPosClauses() const;
        const TrieClauses& getNegClauses() const;


        /* Public Methods */
        // This method will add this clause to the bucket
        void add(const Clause& clause);

        // This method will add this tree to the bucket
        void add(const TrieClauses& tree);

        // This method will retrieve all the clauses in this bucket that contain
        // this literal + modify the size of this bucket
        TrieClauses retrieve(const Literal& literal); 

        // This method will remove everything in the bucket
        void reset();


    private:
        /* Declaring Variables */
        int key;
        TrieClauses posClauses;
        TrieClauses negClauses;
};


/* Inline Functions */
/* Getters & Setters */
inline unsigned Bucket::getSize() const {
    return posClauses.getSize() + negClauses.getSize();
}


inline const int& Bucket::getKey() const {
    return key;
}


inline const TrieClauses& Bucket::getPosClauses() const {
    return posClauses;
}


inline const TrieClauses& Bucket::getNegClauses() const {
    return negClauses;
}

#endif // BUCKET_HPP
