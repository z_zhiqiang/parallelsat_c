#ifndef AVLTREE_HPP
#define AVLTREE_HPP

#include <memory>
#include <stack>
#include <iterator>
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <cmath>


// This enum class is used to detect the direction going back when
// add an element to AVL tree. Basically, it will store which direction
// is going back up from recursive call
enum class Direction {
    Left,
    Right,
    None
};


// Forward declare iterator AVLTreeIter
template <typename AVLType, typename ValueType, typename PointerType>
class AVLTreeIter;


// This class represents an AVL tree where the nodes will be sorted (BST with AVL balancing)
template <typename T>
class AVLTree {

    public:
        /* Declaring Variables */
        // Node struct for the tree containing key and pointers to left and right + its height
        struct Node {
            int height;
            T key;
            std::unique_ptr<Node> left;
            std::unique_ptr<Node> right;
        };


        /* Class Constructors & Destructor */ 
        AVLTree();
        AVLTree(const AVLTree& s);
        virtual ~AVLTree();


        /* Class Operators */
        template <typename U>
        friend std::ostream& operator<<(std::ostream& strm, const AVLTree<U>& tree);

        AVLTree& operator=(const AVLTree& s);

        // Custorm iterator for this AVL tree
        friend class AVLTreeIter<T, T, Node*>;
        friend class AVLTreeIter<T, const T, const Node*>;
        typedef AVLTreeIter<T, T, Node*> iterator;
        typedef AVLTreeIter<T, const T, const Node*> const_iterator;
        iterator begin(); 
        iterator end();
        const_iterator cbegin() const;
        const_iterator cend() const;


        /* Getters & Setters */
        unsigned getSize() const;


        /* Public Methods */
        // add() adds an element to the set.  If the element is already in the set,
        // this function has no effect.  This function always runs in O(log n) time
        // when there are n elements in the AVL tree.
        T& add(const T& element);

        // This method will remove the element and rebalance the tree. This method
        // should always runs in O(log n).
        void remove(const T& element);

        // This method will remove all the elements in this tree
        void reset();

        // contains() returns true if the given element is already in the set,
        // false otherwise.  This function always runs in O(log n) time when
        // there are n elements in the AVL tree.
        bool contains(const T& element) const;

        // This method will find the given element in the AVL tree and return it.
        T& find(const T& element) const;

        // This method will find the given element and pop it out from the tree.
        T pop(const T& element);


    private:
        /* Declaring Variables */
        // unique pointer to node root
        std::unique_ptr<Node> root;

        // variable to store the size of this tree (number of elements)
        unsigned totalSize;


        /* Private Methods */
        // helper function to do copy recursively
        void copyRecursive(const std::unique_ptr<Node>& copyNode, std::unique_ptr<Node>& node) const;

        // helper function to do add recursively
        void addRecursive(const T& element, std::unique_ptr<Node>& node,
            bool& isElementExisted, Direction& onePreDirection, Direction& twoPreDirection, T*& result);

        // helper function to do contains recursively
        bool containsRecursive(const T& element, const std::unique_ptr<Node>& node) const;

        // helper function to do find recursively
        T& findRecursive(const T& element, const std::unique_ptr<Node>& node) const;

        // helper function to do pop recursively
        T popRecursive(const T& element, std::unique_ptr<Node>& node, Direction& onePreDirection, Direction& twoPreDirection);

        // helper function to find the min node based on the root node
        const T minNode(const std::unique_ptr<Node>& node) const;

        // balance the tree using the AVL rotation techniques
        void balanceTree(std::unique_ptr<Node>& node, const Direction& onePreDirection, const Direction& twoPreDirection);

        // helper function to handle rotate LL
        void rotateLL(std::unique_ptr<Node>& nodeRoot, std::unique_ptr<Node>& nodeLeft);

        // helper function to handle rotate RR
        void rotateRR(std::unique_ptr<Node>& nodeRoot, std::unique_ptr<Node>& nodeRight);

        // helper function to handle rotate LR
        void rotateLR(std::unique_ptr<Node>& nodeRoot, std::unique_ptr<Node>& nodeLeft, std::unique_ptr<Node>& nodeRight);

        // helper function to handle rotate RL
        void rotateRL(std::unique_ptr<Node>& nodeRoot, std::unique_ptr<Node>& nodeRight, std::unique_ptr<Node>& nodeLeft);
        
        // traverse the tree in-order style and print
        void traverseInOrder(std::ostream& strm, const std::unique_ptr<Node>& node) const;
};


/* Template Declaration */
/* Class Constructors & Destructor */
template <typename T>
AVLTree<T>::AVLTree()
    : root{nullptr}, totalSize{0} {

}


template <typename T>
AVLTree<T>::AVLTree(const AVLTree& s)
    : totalSize{s.totalSize} {
    // call helper function to do the copy
    this->copyRecursive(s.root, root);
}


template <typename T>
AVLTree<T>::~AVLTree() {

}


/* Class Operators */
template <typename T>
AVLTree<T>& AVLTree<T>::operator=(const AVLTree& s) {
    if (this != &s) {
        // copy totalSize from s
        totalSize = s.totalSize;

        // reset the node root pointer
        root.reset();

        // call helper function to do the assignment/copy
        this->copyRecursive(s.root, root);
    }

    return *this;
}


template <typename T>
std::ostream& operator<<(std::ostream& strm, const AVLTree<T>& tree) {
    // traverse the tree with in-order style
    tree.traverseInOrder(strm, tree.root);    

    return strm;
}


template <typename T>
inline typename AVLTree<T>::iterator AVLTree<T>::begin() { 
    return (totalSize == 0) ? AVLTree<T>::iterator{} : AVLTree<T>::iterator{*this};
}


template <typename T>
inline typename AVLTree<T>::iterator AVLTree<T>::end() {
    return AVLTree<T>::iterator{};
}


template <typename T>
inline typename AVLTree<T>::const_iterator AVLTree<T>::cbegin() const {
    return (totalSize == 0) ? AVLTree<T>::const_iterator{} : AVLTree<T>::const_iterator{*this};
}


template <typename T>
inline typename AVLTree<T>::const_iterator AVLTree<T>::cend() const {
    return AVLTree<T>::const_iterator{};
}


/* Getters & Setters */
template <typename T>
inline unsigned AVLTree<T>::getSize() const {
    return totalSize;
}


/* Public Methods */
template <typename T>
T& AVLTree<T>::add(const T& element) {
    // initialize isElementExisted, onePreDirection, and twoPreDirection
    T* result;
    bool isElementExisted = false;
    Direction onePreDirection = Direction::None;
    Direction twoPreDirection = Direction::None;

    // call helper function to do add
    this->addRecursive(element, root, isElementExisted, onePreDirection, twoPreDirection, result);
    return *result;
}


template <typename T>
void AVLTree<T>::remove(const T& element) {
    // try to remove this element
    try {
        this->pop(element);
    }
    catch (...) {}
}


template <typename T>
void AVLTree<T>::reset() {
    root.reset();
    root = nullptr;
    totalSize = 0;
}


template <typename T>
bool AVLTree<T>::contains(const T& element) const {
    // call helper function to do contains
    return this->containsRecursive(element, root);
}


template <typename T>
T& AVLTree<T>::find(const T& element) const {
    // call helper function to do find
    return this->findRecursive(element, root);
}


template <typename T>
T AVLTree<T>::pop(const T& element) {
    // init variables
    Direction onePreDirection = Direction::None;
    Direction twoPreDirection = Direction::None;

    // reduce the size
    totalSize--;

    // call the helper function to do pop
    return this->popRecursive(element, root, onePreDirection, twoPreDirection);
}



/* Private Methods */
template <typename T>
void AVLTree<T>::copyRecursive(const std::unique_ptr<Node>& copyNode, std::unique_ptr<Node>& node) const {
    /* Base Case Section */
    // handle when the copyNode is empty
    if (copyNode == nullptr) {
        return;
    }

    /* Recursive Call Section */
    // handle creating new Node from copyNode
    node = std::unique_ptr<Node>{new Node{copyNode->height, copyNode->key, nullptr, nullptr}};
    this->copyRecursive(copyNode->left, node->left);
    this->copyRecursive(copyNode->right, node->right);
}


template <typename T>
void AVLTree<T>::addRecursive(const T& element, std::unique_ptr<Node>& node,
    bool& isElementExisted, Direction& onePreDirection, Direction& twoPreDirection, T*& result) {
    /* Base Case Section */
    // handle when the node is empty
    if (node == nullptr) {
        // only add when element is not existed
        if (!isElementExisted) {
            node = std::unique_ptr<Node>{new Node{0, element, nullptr, nullptr}};
            result = &node->key;

            // increase the size (number of elements)
            ++totalSize;
        }
        return;
    }

    // handle when element is equal to the key
    if (element == node->key) {
        isElementExisted = true;
        result = &node->key;
        return;
    }

    /* Recursive Call Section */
    // handle when element is less than the key
    else if (element < node->key) {
        this->addRecursive(element, node->left,
            isElementExisted, onePreDirection, twoPreDirection, result);
        
        // handle assign onePreDirection & twoPreDirection
        twoPreDirection = onePreDirection;
        onePreDirection = Direction::Left;
    }

    // handle when element is greater than the key
    else {
        this->addRecursive(element, node->right,
            isElementExisted, onePreDirection, twoPreDirection, result);

        // handle assign onePreDirection & twoPreDirection
        twoPreDirection = onePreDirection;
        onePreDirection = Direction::Right;
    }

    // handle when finish going down bottom and add
    if (!isElementExisted) {
        this->balanceTree(node, onePreDirection, twoPreDirection);
    }
}


template <typename T>
bool AVLTree<T>::containsRecursive(const T& element, const std::unique_ptr<Node>& node) const {
    /* Base Case Section */
    // handle when the node is empty
    if (node == nullptr) {
        return false;
    }

    // handle when element is equal to the key
    if (element == node->key) {
        return true;
    }

    /* Recursive Call Section */
    // handle when element is less than the key
    else if (element < node->key) {
        return this->containsRecursive(element, node->left);
    }
    // handle when element is greater than the key
    else {
        return this->containsRecursive(element, node->right);
    }
}


template <typename T>
T& AVLTree<T>::findRecursive(const T& element, const std::unique_ptr<Node>& node) const {
    // Base Case
    if ((node == nullptr) || (totalSize == 0)) {
        throw std::out_of_range(std::string("Can't find element in AVL tree"));
    }

    // find the element in the tree
    if (element == node->key) {
        return node->key;
    }
    else if (element < node->key) {
        return this->findRecursive(element, node->left);
    }
    else {
        return this->findRecursive(element, node->right);
    }
}


template <typename T>
T AVLTree<T>::popRecursive(const T& element, std::unique_ptr<Node>& node, Direction& onePreDirection, Direction& twoPreDirection) {
    // Base Case
    if (node == nullptr) {
        // increase the size bc we reduced before this process
        ++totalSize;

        throw std::out_of_range(std::string("Can't remove element in AVL tree"));
    }

    // init the result
    T result;

    // handle when element is less than the key
    if (element < node->key) {
        result = popRecursive(element, node->left, onePreDirection, twoPreDirection);
        twoPreDirection = onePreDirection;
        onePreDirection = Direction::Left;
    }

    // handle when element is greater than the key
    else if (element > node->key) {
        result = popRecursive(element, node->right, onePreDirection, twoPreDirection);
        twoPreDirection = onePreDirection;
        onePreDirection = Direction::Right;
    }

    // handle when element is equal to the key
    else {
        result = node->key;

        // handle when node has only one child or no child
        if ((node->left == nullptr) || (node->right == nullptr)) {
            // move the child to temp
            std::unique_ptr<Node> temp;
            if (node->left != nullptr) {
                temp = std::move(node->left);
                node->left.reset();

                // swap the existed child to root 
                node = std::move(temp); 
            }
            else if (node->right != nullptr) {
                temp = std::move(node->right);
                node->right.reset();

                // swap the existed child to root 
                node = std::move(temp);
            }
            else {
                node.reset();
                return result;
            }
        }

        // handle when node has two children
        else {
            // get the inorder successor (smallest item in the right subtree)
            node->key = this->minNode(node->right);

            // delete the inorder successor node
            popRecursive(node->key, node->right, onePreDirection, twoPreDirection);
        }
    }

    // balance the tree while backtracking back
    this->balanceTree(node, onePreDirection, twoPreDirection);

    return result;
}


template <typename T>
const T AVLTree<T>::minNode(const std::unique_ptr<Node>& node) const {
    // Base Case
    if (node->left == nullptr) {
        return node->key;
    }

    // go through the left for min node
    return this->minNode(node->left);
}


template <typename T>
void AVLTree<T>::balanceTree(std::unique_ptr<Node>& node, const Direction& onePreDirection, const Direction& twoPreDirection) {
    // handle the height of this node
    int leftHeight = (node->left == nullptr) ? -1 : node->left->height;
    int rightHeight = (node->right == nullptr) ? -1 : node->right->height;
    node->height = std::max(leftHeight, rightHeight) + 1;

    // handle unbalanced heights
    if (std::abs(leftHeight - rightHeight) > 1) {
        switch (onePreDirection) {
        // handle when onePreDirection = Left
        case Direction::Left:
            switch (twoPreDirection) {
                case Direction::Left:
                    this->rotateLL(node, node->left);
                    break;

                case Direction::Right:
                    this->rotateLR(node, node->left, node->left->right);
                    break;

                case Direction::None:
                    return;
            }
            break;

        // handle when onePreDirection = Right
        case Direction::Right:
            switch (twoPreDirection) {
                case Direction::Left:
                    this->rotateRL(node, node->right, node->right->left);
                    break;

                case Direction::Right:
                    this->rotateRR(node, node->right);
                    break;

                case Direction::None:
                    return;
            }
            break;

        // handle when oneDirection = None (default)
        default:
            return;
        }
    }
}


template <typename T>
void AVLTree<T>::rotateLL(std::unique_ptr<Node>& nodeRoot, std::unique_ptr<Node>& nodeLeft) {
    // move the nodeLeft->right to a tempNodeLeftRight pointer + reset the nodeLeft->right
    std::unique_ptr<Node> tempNodeLeftRight = std::move(nodeLeft->right);
    nodeLeft->right.reset();

    // move the nodeLeft to a tempNodeLeft pointer + reset nodeRoot->left + nodeLeft
    std::unique_ptr<Node> tempNodeLeft = std::move(nodeLeft);
    nodeRoot->left.reset();
    nodeLeft.reset();

    // move the nodeRoot to a tempNodeRoot pointer + reset nodeRoot
    std::unique_ptr<Node> tempNodeRoot = std::move(nodeRoot);
    nodeRoot.reset();

    // assign nodeRoot with tempNodeLeft + nodeRoot->right to tempNodeRoot
    // + nodeRoot->right->left to tempNodeLeftRight
    nodeRoot = std::move(tempNodeLeft);
    nodeRoot->right = std::move(tempNodeRoot);
    nodeRoot->right->left = std::move(tempNodeLeftRight);

    // adjust the height of nodeRoot->right
    int heightRootRL = (nodeRoot->right->left == nullptr) ? -1 : nodeRoot->right->left->height;
    int heightRootRR = (nodeRoot->right->right == nullptr) ? -1 : nodeRoot->right->right->height;
    nodeRoot->right->height = std::max(heightRootRL, heightRootRR) + 1;

    // adjust the height of nodeRoot
    int heightRootL = (nodeRoot->left == nullptr) ? -1 : nodeRoot->left->height;
    int heightRootR = (nodeRoot->right == nullptr) ? -1 : nodeRoot->right->height;
    nodeRoot->height = std::max(heightRootL, heightRootR) + 1;
}


template <typename T>
void AVLTree<T>::rotateRR(std::unique_ptr<Node>& nodeRoot, std::unique_ptr<Node>& nodeRight) {
    // move the nodeRight->left  to a tempNodeRightLeft pointer + reset nodeRight->left
    std::unique_ptr<Node> tempNodeRightLeft = std::move(nodeRight->left);
    nodeRight->left.reset();

    // move the nodeRight to a tempNodeRight pointer + reset nodeRoot->right + nodeRight
    std::unique_ptr<Node> tempNodeRight = std::move(nodeRight);
    nodeRoot->right.reset();
    nodeRight.reset();

    // move the nodeRoot to a tempNodeRoot pointer + reset nodeRoot
    std::unique_ptr<Node> tempNodeRoot = std::move(nodeRoot);
    nodeRoot.reset();

    // assign nodeRoot with tempNodeRight + nodeRoot->left to tempNodeRoot
    // + nodeRoot->left->right to tempNodeRightLeft
    nodeRoot = std::move(tempNodeRight);
    nodeRoot->left = std::move(tempNodeRoot);
    nodeRoot->left->right = std::move(tempNodeRightLeft);

    // adjust the height of nodeRoot->left
    int heightRootLL = (nodeRoot->left->left == nullptr) ? -1 : nodeRoot->left->left->height;
    int heightRootLR = (nodeRoot->left->right == nullptr) ? -1 : nodeRoot->left->right->height;
    nodeRoot->left->height = std::max(heightRootLL, heightRootLR) + 1;

    // adjust the height of nodeRoot
    int heightRootL = (nodeRoot->left == nullptr) ? -1 : nodeRoot->left->height;
    int heightRootR = (nodeRoot->right == nullptr) ? -1 : nodeRoot->right->height;
    nodeRoot->height = std::max(heightRootL, heightRootR) + 1;
}


template <typename T>
void AVLTree<T>::rotateLR(std::unique_ptr<Node>& nodeRoot, std::unique_ptr<Node>& nodeLeft, std::unique_ptr<Node>& nodeRight) {
    this->rotateRR(nodeLeft, nodeRight);
    this->rotateLL(nodeRoot, nodeLeft);
}


template <typename T>
void AVLTree<T>::rotateRL(std::unique_ptr<Node>& nodeRoot, std::unique_ptr<Node>& nodeRight, std::unique_ptr<Node>& nodeLeft) {
    this->rotateLL(nodeRight, nodeLeft);
    this->rotateRR(nodeRoot, nodeRight);
}


template <typename T>
void AVLTree<T>::traverseInOrder(std::ostream& strm, const std::unique_ptr<Node>& node) const {
    // Base Case
    if (node == nullptr) {
        return;
    }

    this->traverseInOrder(strm, node->left);
    strm << node->key << " (height = " << node->height << ")" << std::endl;
    this->traverseInOrder(strm, node->right);
}


/* AVLTreeIter Class Declaration */
// This class reprensents the iterator for the AVL tree (only support in-order traversal right now)
template <typename AVLType, typename ValueType, typename PointerType>
class AVLTreeIter : public std::iterator<std::input_iterator_tag, ValueType> {

    public:
        /* Class Constructors & Destructor */
        AVLTreeIter();
        AVLTreeIter(const AVLTree<AVLType>& tree);
        virtual ~AVLTreeIter();


        /* Class Operators */
        template <typename A, typename B, typename C>
        friend bool operator==(const AVLTreeIter<A,B,C>& lhs, const AVLTreeIter<A,B,C>& rhs);

        template <typename A, typename B, typename C>
        friend bool operator!=(const AVLTreeIter<A,B,C>& lhs, const AVLTreeIter<A,B,C>& rhs);

        AVLTreeIter& operator++();
        AVLTreeIter operator++(int);
        ValueType& operator*();
        PointerType operator->();


    private:
        /* Declaring Variables */
        // a struct to memorize the stages
        struct Stage {
            bool isVisited;
            Direction direction;
            PointerType node;
        };

        std::stack<Stage> nodeStack;
        PointerType current;

        /* Private Methods */
        // helper function to get the min value
        void getMinValue();

        // helper function to get the next node
        void getNext();
};


/* Template Declaration */
/* Class Constructors & Destructor */
template <typename AVLType, typename ValueType, typename PointerType>
AVLTreeIter<AVLType,ValueType,PointerType>::AVLTreeIter() 
    : current{nullptr} {

}


template <typename AVLType, typename ValueType, typename PointerType>
AVLTreeIter<AVLType,ValueType,PointerType>::AVLTreeIter(const AVLTree<AVLType>& tree) 
    : nodeStack{} {
    // get the current node from the tree
    nodeStack.push(Stage{false, Direction::Left, tree.root.get()});
    getMinValue();
    nodeStack.top().isVisited = true;
}


template <typename AVLType, typename ValueType, typename PointerType>
AVLTreeIter<AVLType,ValueType,PointerType>::~AVLTreeIter() {

}


/* Class Operators */
template <typename AVLType, typename ValueType, typename PointerType>
inline bool operator==(const AVLTreeIter<AVLType,ValueType,PointerType>& lhs, const AVLTreeIter<AVLType,ValueType,PointerType>& rhs) {
    return lhs.current == rhs.current;
}


template <typename AVLType, typename ValueType, typename PointerType>
inline bool operator!=(const AVLTreeIter<AVLType,ValueType,PointerType>& lhs, const AVLTreeIter<AVLType,ValueType,PointerType>& rhs) {
    return lhs.current != rhs.current;
}


template <typename AVLType, typename ValueType, typename PointerType>
inline AVLTreeIter<AVLType,ValueType,PointerType>& AVLTreeIter<AVLType,ValueType,PointerType>::operator++() {
    getNext();
    return *this;
}


template <typename AVLType, typename ValueType, typename PointerType>
AVLTreeIter<AVLType,ValueType,PointerType> AVLTreeIter<AVLType,ValueType,PointerType>::operator++(int) {
    AVLTreeIter<AVLType,ValueType,PointerType> tmp{*this};
    operator++();
    return tmp;
}


template <typename AVLType, typename ValueType, typename PointerType>
inline ValueType& AVLTreeIter<AVLType,ValueType,PointerType>::operator*() {
    return current->key;
}


template <typename AVLType, typename ValueType, typename PointerType>
inline PointerType AVLTreeIter<AVLType,ValueType,PointerType>::operator->() {
    return current;
}


/* Private Methods */
template <typename AVLType, typename ValueType, typename PointerType>
void AVLTreeIter<AVLType,ValueType,PointerType>::getMinValue() {
    // Base Case
    PointerType topElement = nodeStack.top().node;
    if (topElement->left == nullptr) {
        current = topElement;
        return;
    }
    
    // go to the left and push node on stack
    nodeStack.push(Stage{false, Direction::Left, topElement->left.get()});
    getMinValue();
}


template <typename AVLType, typename ValueType, typename PointerType>
void AVLTreeIter<AVLType,ValueType,PointerType>::getNext() {
    // handle when there is nothing in the stack
    if (nodeStack.size() == 0) {
        current = nullptr;
        return;
    }

    // get the top element
    Stage topElement = nodeStack.top();

    // handle the direction of the topElement
    switch (topElement.direction) {
        case Direction::Left:
            // handle when the node is not visited
            if (!topElement.isVisited) {
                current = topElement.node;
                nodeStack.top().isVisited = true;
                return;
            }

            // handle the right child (isVisited == true)
            if (topElement.node->right == nullptr) {
                // go back one node
                nodeStack.pop();
                getNext();
            }
            else {
                // push the right node on the stack and get the min value
                nodeStack.top().direction = Direction::Right;
                nodeStack.push(Stage{false, Direction::Left, topElement.node->right.get()});
                getMinValue();
                nodeStack.top().isVisited = true;
            }
            break;

        case Direction::Right:
            // pop stack and repeat getNext()
            nodeStack.pop();
            getNext();
            break;

        default:
            throw std::logic_error("Should not have Direction::None here");            
    } 
}

#endif // AVLTREE_HPP
