#include "Clause.hpp"


/* Class Constructors & Destructor */
Clause::Clause() {

}


Clause::Clause(const std::vector<Literal>& clause) {
    // copy data
    size = clause.size();
    key = clause[0].getLiteral();

    // copy the vector to the array
    literals = new Literal[size];
    for (unsigned i = 0; i < size; ++i) {
        literals[i] = clause[i];
    }
}


Clause::Clause(const int literals[], const unsigned& size, const int& key) 
    : size{size}, key{key} {
    // init the literals pointer
    this->literals = new Literal[size];
    for (unsigned i = 0; i < size; ++i) {
        this->literals[i] = literals[i];
    }
}


Clause::Clause(Literal* literals, const unsigned& size, const int& key) 
    : literals{literals}, size{size}, key{key} {

}


Clause::~Clause() {
    delete[] literals;
}


/* Class Operators */
Clause& Clause::operator=(const Clause& clause) {
    if (this == &clause) {
        return *this;
    }

    size = clause.size;
    key = clause.key;
    for (unsigned i = 0; i < clause.getSize(); ++i) {
        literals[i] = clause.literals[i];
    }
    return *this;
}


std::ostream& operator<<(std::ostream& strm, const Clause& clause) {
    // print out the fields
    strm << "Clause (key = " << clause.key << ", size = " << clause.size << ") = [";

    // print out the literals
    for (unsigned i = 0; i < (clause.size - 1); ++i) {
        strm << clause.literals[i] << ", ";
    }
    strm << clause.literals[clause.size-1] << "]" << std::endl;

    return strm;
}


/* Public Methods */
// This method will check whether the clause is sorted or not based on the orderings (for debugging)
bool Clause::isSorted(const Clause& clause) {
    // loop through the clause and expected it to be ascending
    for (Clause::const_iterator iter = clause.cbegin(); iter != (clause.cend() - 1); ++iter) {
        if (*iter > *(iter+1)) {
            return false;
        }
    }

    return true;
}
