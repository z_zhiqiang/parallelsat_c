#ifndef TRIECLAUSES_HPP
#define TRIECLAUSES_HPP

#include <memory>
#include <vector>
#include <map>

#include "../AbstractDataStructure.hpp"
#include "Clause.hpp"
#include "AVLTree.hpp"


// Forward declare TrieNodePointer
class TrieNodePointer;


// This class represents a set of clauses as a sorted trie data structure
class TrieClauses : AbstractDataStructure {
    
    public:
        /* Declaring Variables */
        // Node struct represents the tree node for this TrieClauses class
        struct Node {
            Literal literal;
            AVLTree<TrieNodePointer> children;
            unsigned size;
        };


        /* Class Constructors & Destructor */
		TrieClauses();
        TrieClauses(const int& key);
        TrieClauses(const Literal& literal);
        TrieClauses(const TrieClauses& tree);
        virtual ~TrieClauses();


        /* Class Operators */
        friend std::ostream& operator<<(std::ostream& strm, const TrieClauses& trieClauses);
        TrieClauses& operator=(const TrieClauses& tree);


        /* Getters & Setters */
        const Node* get() const;
        const int& getKey() const; 
        unsigned getSize() const;


        /* Public Methods */
        // This method will add a clause to the tree
        void add(const Clause& clause);

        // This method will add another trie into current trie
        void add(const TrieClauses& tree);

        // This method will retrieve all the clauses with given specific literal 
        // in this tree and remove these clauses from the tree
        AbstractDataStructure* retrieve(const Literal& literal);

        // This method will do the resolution with a clause from this tree
        std::map<int,AbstractDataStructure> doResolution(const Clause& clause) const; 

        // This method will remove everything in this tree
        void reset();


    private:
        /* Declaring Variables */
        std::unique_ptr<Node> root;
        int key;


        /* Private Methods */
        // helper function to copy the object recursively
        void copyRecursive(const TrieClauses& tree);

        // helper function to add the clause recursively
        void addRecursive(const Clause& clause, Node* node, unsigned clauseIndex, int& removed, bool prevFound);

        // helper function to add the children recursively
        void addRecursive(const AVLTree<TrieNodePointer>& children, Node* node, int& removed, bool prevFound);

        // helper function to retrieve clauses containing given literal
        void retrieveRecursive(Node* node, std::vector<Literal>& clause, TrieClauses& result, int& removed, bool isLiteralFound);
        
        // helper function to do the resolution recursively given the clause
        void doResolutionRecursive(const Clause& clause, const Node* node, unsigned clauseIndex, std::map<int,TrieClauses>& result, std::vector<Literal>& resolutionClause) const;

        // helper function to print all the clauses in the tree (for operator<<)
        void printTree(std::ostream& strm, const AVLTree<TrieNodePointer>& children, std::vector<Literal>& literals) const;
};


/* Inline Functions */
/* Getters & Setters */
inline const TrieClauses::Node* TrieClauses::get() const {
    return root.get();
}


inline const int& TrieClauses::getKey() const {
    return key;
}


inline unsigned TrieClauses::getSize() const {
    // DEBUG ONLY
    std::cout << "key" << std::endl;
    std::cout << key << std::endl;
    std::cout << ((root == nullptr) ? "root null" : "root not null") << std::endl;

    return root ? root->size : 0;
}


/* TrieNodePointer Declaration */
// This class is a wrapper of the pointers to the node in TrieClauses (created mainly for comparison)
class TrieNodePointer {

    public:
        /* Class Constructors & Destructor */
        TrieNodePointer();
        TrieNodePointer(TrieClauses::Node* rawPtr);
        TrieNodePointer(const Literal& literal);
        TrieNodePointer(const TrieNodePointer& ptr);
        virtual ~TrieNodePointer();


        /* Class Operators */
        friend std::ostream& operator<<(std::ostream& strm, const TrieNodePointer& ptr);
        TrieNodePointer& operator=(const TrieNodePointer& ptr);
        friend bool operator==(const TrieNodePointer& lhs, const TrieNodePointer& rhs);
        friend bool operator!=(const TrieNodePointer& lhs, const TrieNodePointer& rhs);
        friend bool operator<(const TrieNodePointer& lhs, const TrieNodePointer& rhs);
        friend bool operator>(const TrieNodePointer& lhs, const TrieNodePointer& rhs);


        /* Getters & Setters */
        TrieClauses::Node* get() const;
        Literal getLiteral() const;
        unsigned getSize() const;


    private:
        /* Declaring Variables */
        std::unique_ptr<TrieClauses::Node> ptr;

};


/* Inline Functions */
/* Class Operators */
inline std::ostream& operator<<(std::ostream& strm, const TrieNodePointer& ptr) {
    return strm << "pointer to literal " << ptr.getLiteral() << std::endl;
}


inline bool operator==(const TrieNodePointer& lhs, const TrieNodePointer& rhs) {
    return lhs.getLiteral() == rhs.getLiteral();
}


inline bool operator!=(const TrieNodePointer& lhs, const TrieNodePointer& rhs) {
    return lhs.getLiteral() != rhs.getLiteral();
}


inline bool operator<(const TrieNodePointer& lhs, const TrieNodePointer& rhs) {
    return lhs.getLiteral() < rhs.getLiteral();
}


inline bool operator>(const TrieNodePointer& lhs, const TrieNodePointer& rhs) {
    return lhs.getLiteral() > rhs.getLiteral();
}


/* Getters & Setters */
inline TrieClauses::Node* TrieNodePointer::get() const {
    return ptr.get();
}


inline Literal TrieNodePointer::getLiteral() const {
    return ptr ? ptr->literal : Literal{0};
}


inline unsigned TrieNodePointer::getSize() const {
    return ptr ? ptr->size : 0;
}

#endif // TRIECLAUSES_HPP
