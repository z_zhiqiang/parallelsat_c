#include "LiteralCounts.hpp"


/* Class Constructors & Destructor */
LiteralCounts::LiteralCounts() 
    : counts{nullptr}, minBucket{-1}, maxBucket{-1}, size{0} {

}


LiteralCounts::LiteralCounts(const int& numOfVariables) 
    : minBucket{-1}, maxBucket{-1}, size{numOfVariables} {
    // init the counts
    int* data = new int[numOfVariables];
    for (int i = 0; i < numOfVariables; ++i) {
        data[i] = 0;
    }
    counts = std::unique_ptr<int[]>{data};
}


LiteralCounts::~LiteralCounts() {

}


/* Class Operators */
std::ostream& operator<<(std::ostream& strm, const LiteralCounts& literalCounts) {
    strm << "Counts (size = " << literalCounts.size << "):" << std::endl;
    
    // print the counts
    for (int i = 0; i < literalCounts.size; ++i) {
        strm << "Variable " << (i + 1) << " = " << literalCounts.counts[i] << std::endl;
    }

    return strm;
}


LiteralCounts& LiteralCounts::operator=(const LiteralCounts& counts) {
    if (this != &counts) {
        minBucket = counts.minBucket;
        maxBucket = counts.maxBucket;
        size = counts.size;

        // populate this counts
        this->counts.reset();
        int* newData = new int[size];
        for (int i = 0; i < size; ++i) {
            newData[i] = counts.counts[i];
        }
        this->counts = std::unique_ptr<int[]>{newData};
    }
    
    return *this;
}


/* Getters & Setters */
int LiteralCounts::getMin() {
    // DEBUG ONLY
    std::cout << "minBucket = " << minBucket << std::endl;

    // TODO: Need to find a better way to do this...
    if (minBucket != -1) {
        for (int i = 0; i < size; ++i) {
            if (counts[minBucket-1] == 0 || counts[minBucket-1] > counts[i]) {
                minBucket = i + 1;
            }
        }
        return minBucket;
    } else {
        return -1;   
    }
}


int LiteralCounts::getMax() {
    // TODO: Need to find a better way to do this...
    if (maxBucket != -1) {
        for (int i = 0; i < size; ++i) {
            if (counts[maxBucket-1] == 0 || counts[maxBucket-1] <= counts[i]) {
                maxBucket = i + 1;
            }
        }
        return maxBucket;
    } else {
        return -1;
    }
}


/* Public Methods */
void LiteralCounts::add(const TrieClauses& tree) {
    // handle when tree is empty
    if (tree.getSize() == 0) {
        return;
    }

    // call helper function to add the counts from this tree
    updateRecursive(tree.get(), true);
}


void LiteralCounts::add(const Bucket& bucket) {
    // add posClauses and negClauses
    add(bucket.getPosClauses());
    add(bucket.getNegClauses());
}


void LiteralCounts::remove(const TrieClauses& tree) {
    // handle when tree is empty
    if (tree.getSize() == 0) {
        return;
    }

    // call helper function to remove the counts from this tree
    updateRecursive(tree.get(), false);
}


void LiteralCounts::remove(const Bucket& bucket) {
    // remove posClauses and negClauses
    remove(bucket.getPosClauses());
    remove(bucket.getNegClauses());
}


/* Private Methods */
void LiteralCounts::updateRecursive(const TrieClauses::Node* node, const bool& isAdd) {
    int nodeIndex = std::abs(node->literal.getLiteral()) - 1;
       
    // Base Case (when the node doesn't have any children)
    if (node->children.getSize() == 0) {
        // update the count
        if (isAdd) {
            counts[nodeIndex] += node->size;
        }
        else {
            counts[nodeIndex] -= node->size;
        }
        updateMinMax(nodeIndex);
        return;
    }

    // loop through the node children and update the node while backtracking
    for (auto iter = node->children.cbegin(); iter != node->children.cend(); ++iter) {
        updateRecursive((*iter).get(), isAdd);
    }

    // update the count while backtracking
    if (isAdd) {
        counts[nodeIndex] += node->size;
    }
    else {
        counts[nodeIndex] -= node->size;
    }
    updateMinMax(nodeIndex);
}


void LiteralCounts::updateMinMax(const int& index) {
    // handle min and max not initialized
    if (minBucket == -1) {
        minBucket = index + 1;
    }
    else {
        // update min & handle 0
        if ((counts[minBucket-1] >= counts[index]) && (counts[index] != 0)) {
            minBucket = index + 1;
        }
    }

    if (maxBucket == -1) {
        maxBucket = index + 1;
    }
    else {
        // update max & handle 0
        if ((counts[maxBucket-1] <= counts[index]) && (counts[index] != 0)) {
            maxBucket = index + 1;
        }
    }
}
