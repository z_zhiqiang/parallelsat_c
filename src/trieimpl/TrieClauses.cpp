#include "TrieClauses.hpp"


/* TrieClauses Declaration */
/* Class Constructors & Destructor */
TrieClauses::TrieClauses() 
    : root{nullptr}, key{0} {

}


TrieClauses::TrieClauses(const int& key) 
    : root{nullptr}, key{key} {

}


TrieClauses::TrieClauses(const Literal& literal) 
    : root{nullptr}, key{literal.getLiteral()} {

}


TrieClauses::TrieClauses(const TrieClauses& tree) {
    // call helper function to copy
    copyRecursive(tree);
}


TrieClauses::~TrieClauses(){

}


/* Class Operators */
std::ostream& operator<<(std::ostream& strm, const TrieClauses& trieClauses) {
    // don't handle when this trie is empty
    if (trieClauses.root == nullptr) {
        return strm << "Trie is empty" << std::endl;
    }

    strm << "Trie " << trieClauses.key << " -- size = " << trieClauses.getSize() << std::endl;

    // print the clauses in the tree
    std::vector<Literal> literals{};
    literals.push_back(trieClauses.root->literal);
    trieClauses.printTree(strm, trieClauses.root->children, literals);

    return strm;
}


TrieClauses& TrieClauses::operator=(const TrieClauses& tree) {
    if (this != &tree) {
        // call helper function to copy
        copyRecursive(tree);
    }

    return *this;
}


/* Public Methods */
void TrieClauses::add(const Clause& clause) {
    // handle when the root is nullptr
    if (root == nullptr) {
        root = std::unique_ptr<Node>{new Node{clause.get(0), AVLTree<TrieNodePointer>{}, 0}};
        key = clause.getKey();
    }

    // double check the key
    assert((key == clause.getKey()) && "Adding wrong clause to a trie");

    // call helper function to add this clause
    int removed = 0;
    addRecursive(clause, root.get(), 1, removed, false);
}


void TrieClauses::add(const TrieClauses& tree) {
    // handle when the root is nullptr
    if (root == nullptr) {
        copyRecursive(tree);
        return;
    }

    // double check the key
    assert((key == tree.getKey()) && "Adding wrong tries with different keys");

    // call helper function to add this tree
    int removed = 0;
    addRecursive(tree.get()->children, root.get(), removed, false);
}


TrieClauses TrieClauses::retrieve(const Literal& literal) {
    // handle when literal is equal to root
    if (root->literal == literal) {
        return *this;
    }

    // init necessary variables
    TrieClauses result{literal};
    std::vector<Literal> clause{};
    clause.push_back(literal);
    clause.push_back(root->literal);

    // call helper function to retrieve recursively
    int removed = 0;
    retrieveRecursive(root.get(), clause, result, removed, false);

    return result;
}


std::map<int,TrieClauses> TrieClauses::doResolution(const Clause& clause) const {
    // Base Case (first items must be opposite literal)
    assert((clause.get(0) != root->literal) && "Doing resolution on the wrong tree");

    // init the result
    std::map<int,TrieClauses> result{};
    std::vector<Literal> literals{};

    // call helper function to do resolution recursively
    doResolutionRecursive(clause, root.get(), 1, result, literals);
 
    return result;
}


void TrieClauses::reset() {
    root.reset();
    key = 0;
}


/* Private Methods */
void TrieClauses::copyRecursive(const TrieClauses& tree) {
    this->key = tree.key;
    
    // make a copy of root with new objects
    root = std::unique_ptr<Node>{new Node{tree.root->literal, tree.root->children, tree.root->size}};
}


void TrieClauses::addRecursive(const Clause& clause, Node* node, unsigned clauseIndex, int& removed, bool prevFound) { 
    // Base Case (handle clauseIndex out of range)
    if (clauseIndex >= clause.getSize()) {
        // add or remove the size
        removed = (node->size > 0) ? (node->size - 1) : -1;
        node->size = 1;

        // remove the children of this node (subsumption process)
        node->children.reset();

        return;
    }

    // create a temp pointer of the current literal
    TrieNodePointer tmp = TrieNodePointer{clause.get(clauseIndex)};

    // try to find the literal in the node children
    try {
        TrieNodePointer& element = node->children.find(tmp);
        addRecursive(clause, element.get(), clauseIndex + 1, removed, true);
    }
    catch (...) {
        // can't find the literal
        // only add when this node has at least one child -- not leaf (subsumption)
        // or when previous node is not found and can't find the literal now also
        if (!prevFound || (node->children.getSize() > 0)) {
            TrieNodePointer& element = node->children.add(tmp);
            addRecursive(clause, element.get(), clauseIndex + 1, removed, false);
        }
    }

    // remove the size if there is any removed
    node->size -= removed;
}


void TrieClauses::addRecursive(const AVLTree<TrieNodePointer>& children, Node* node, int& removed, bool prevFound) {
    // Base Case (handle when there is nothing in the children)
    if (children.getSize() == 0) {
        // add or remove the size
        removed = (node->size > 0) ? (node->size - 1) : -1;
        node->size = 1;       

        // remove the children of this node (subsumption process)
        node->children.reset();

        return;
    }

    // loop through the children
    for (AVLTree<TrieNodePointer>::const_iterator iter = children.cbegin(); iter != children.cend(); ++iter) {
        // try to find the literal in the node children
        try {
            TrieNodePointer& element = node->children.find(*iter);
            addRecursive((*iter).get()->children, element.get(), removed, true);
        }
        catch (...) {
            // can't find the literal
            // only add when this node has at least one child -- not leaf (subsumption)
            // or when previous node is not found and can't find the literal now also
            if (!prevFound || (node->children.getSize() > 0)) {
                TrieNodePointer& element = node->children.add(TrieNodePointer{(*iter).getLiteral()});
                addRecursive((*iter).get()->children, element.get(), removed, false);
            }
        }

        // remove the size if there is any removed
        node->size -= removed;
    }
}


void TrieClauses::retrieveRecursive(Node* node, std::vector<Literal>& clause, TrieClauses& result, int& removed, bool isLiteralFound) {
    // Base Case (handle when we reach the leaves)
    if (node->children.getSize() == 0) {
        // only add when the literal is found
        if (isLiteralFound) {
            result.add(Clause{clause});
        }
        return;
    }

    // loop through the current node children ascendingly
    int currChildSize, currChildChildrenSize;
    for (AVLTree<TrieNodePointer>::iterator iter = node->children.begin(); iter != node->children.end(); ++iter) {
        // break when greater than the retrieving literal and literal is not found on the always
        if (!isLiteralFound && (clause[0] < (*iter).getLiteral())) {
            break;
        }

        // assign isLiteralFound & clause
        bool isEqualToLiteral = (clause[0] == (*iter).getLiteral());
        isLiteralFound = isLiteralFound || isEqualToLiteral;
        if (!isEqualToLiteral) {
            clause.push_back((*iter).getLiteral());
        }

        // cache the current child size
        currChildSize = (*iter).getSize();
        currChildChildrenSize = (*iter).get()->children.getSize();
        
        // keep looking to the leaves
        retrieveRecursive((*iter).get(), clause, result, removed, isLiteralFound);

        // backtracking stage
        // reduce the size and remove the branch
        if (isEqualToLiteral) {
            node->children.remove(*iter);
            node->size -= currChildSize;
            break;
        }

        // pop the child
        clause.pop_back();

        // update the size with the new children sizes
        node->size -= currChildSize - (*iter).getSize();

        // remove children that have no children and are not leaves
        if (((*iter).get()->children.getSize() == 0) && (currChildChildrenSize != 0)) {
            node->children.remove(*iter);
        }
    }
}


void TrieClauses::doResolutionRecursive(const Clause& clause, const Node* node, unsigned clauseIndex, std::map<int,TrieClauses>& result, std::vector<Literal>& resolutionClause) const {
    // Base Case (handle when we reach the leaves)
    if (node->children.getSize() == 0) {
        int pushCount = 0;

        // populate the resolution clause if we don't have enough items
        while ((clauseIndex < clause.getSize()) && (node->literal > clause.get(clauseIndex))) {
            resolutionClause.push_back(clause.get(clauseIndex));
            ++clauseIndex;
            ++pushCount;
        }

        // handle when current node is opposite with the last literal
        if (Literal::isOpposite(resolutionClause.back(), node->literal)) {
            // pop the items out
            for (int i = 0; i < pushCount; ++i) {
                resolutionClause.pop_back();
            }
            return;
        }

        // push the node if it doesn't exist in the resolution clause
        if (resolutionClause.back() != node->literal) {
            resolutionClause.push_back(node->literal);
            ++pushCount;
        }

        // add the rest in clause
        while (clauseIndex < clause.getSize()) {
            resolutionClause.push_back(clause.get(clauseIndex));
            ++clauseIndex;
            ++pushCount;
        }

        // add the resolution clause to the right trieclauses
        int key = resolutionClause.front().getLiteral();
        auto it = result.find(key);
        if (it != result.end()) {
            result[key].add(Clause{resolutionClause});
        } else {
            TrieClauses value{key};
            value.add(Clause{resolutionClause});
            result[key] = value;
        }

        // pop the items out
        for (int i = 0; i < pushCount; ++i) {
            resolutionClause.pop_back();
        }

        return;
    }

    // loop through each child in the current node
    for (auto iter = node->children.cbegin(); iter != node->children.cend(); ++iter) {
        // Base Case (when the child is opposite with the current one)
        if (Literal::isOpposite(clause.get(clauseIndex), (*iter).getLiteral())) {
            continue;
        }

        // handle when index is out of range
        if (clauseIndex >= clause.getSize()) {
            // add the items to the clause
            resolutionClause.push_back((*iter).getLiteral());

            // keep going down
            doResolutionRecursive(clause, (*iter).get(), clauseIndex, result, resolutionClause);

            // pop the item out
            resolutionClause.pop_back();
        } else {
            // push the item to the resolution clause
            if ((*iter).getLiteral() < clause.get(clauseIndex)) {
                resolutionClause.push_back((*iter).getLiteral());

                // keep going down
                doResolutionRecursive(clause, (*iter).get(), clauseIndex, result, resolutionClause);

                // pop the item out
                resolutionClause.pop_back();
            } else if ((*iter).getLiteral() == clause.get(clauseIndex)) {
                resolutionClause.push_back((*iter).getLiteral());

                // keep going down & increase index
                doResolutionRecursive(clause, (*iter).get(), ++clauseIndex, result, resolutionClause);

                // pop the item out
                resolutionClause.pop_back();
            } else {
                // add the clause literals until they reach over the current literal
                int pushCount = 0;
                while ((clauseIndex < clause.getSize()) && ((*iter).getLiteral() > clause.get(clauseIndex))) {
                    resolutionClause.push_back(clause.get(clauseIndex));
                    ++clauseIndex;
                    ++pushCount;
                }

                // handle when add all the literals in clause
                if (clauseIndex >= clause.getSize()) {
                    // check whether the current literal is opposite with the last literal
                    if (Literal::isOpposite(clause.get(clauseIndex-1), (*iter).getLiteral())) {
                        // pop out all the pushed items and skip
                        for (int i = 0; i < pushCount; ++i) {
                            resolutionClause.pop_back();
                        }
                        continue;
                    }
                }

                // push the node if it doesn't exist in the resolution clause
                if (resolutionClause.back() != (*iter).getLiteral()) {
                    resolutionClause.push_back((*iter).getLiteral());
                    ++pushCount;
                }
                     
                // keep going down
                doResolutionRecursive(clause, (*iter).get(), clauseIndex, result, resolutionClause);

                // pop the items out
                for (int i = 0; i < pushCount; ++i) {
                    resolutionClause.pop_back();
                }
            }
        }
    }
}


void TrieClauses::printTree(std::ostream& strm, const AVLTree<TrieNodePointer>& children, std::vector<Literal>& literals) const {
    // Base Case
    if (children.getSize() == 0) {
        // print out the clause
        for (std::vector<Literal>::iterator iter = literals.begin(); iter != literals.end(); ++iter) {
            strm << *iter << " ";
        }
        strm << std::endl;
        return;
    }

    // loop through the children and keep going down
    for (AVLTree<TrieNodePointer>::const_iterator iter = children.cbegin(); iter != children.cend(); ++iter) {
        // put children on the right position in the array
        literals.push_back((*iter).getLiteral());
        printTree(strm, (*iter).get()->children, literals);

        // pop out this literal cause this branch is printed
        literals.pop_back();
    }
}


/* TrieNodePointer Declaration */
/* Class Constructors & Destructor */
TrieNodePointer::TrieNodePointer()
    : ptr{nullptr} {

}


TrieNodePointer::TrieNodePointer(TrieClauses::Node* rawPtr) {
    ptr = std::unique_ptr<TrieClauses::Node>{rawPtr};
}


TrieNodePointer::TrieNodePointer(const Literal& literal) {
    ptr = std::unique_ptr<TrieClauses::Node>{new TrieClauses::Node{literal, AVLTree<TrieNodePointer>{}, 0}};
}


TrieNodePointer::TrieNodePointer(const TrieNodePointer& ptr) {
    this->ptr = std::unique_ptr<TrieClauses::Node>{new TrieClauses::Node{ptr.getLiteral(), ptr.get()->children, ptr.getSize()}};
}


TrieNodePointer::~TrieNodePointer() {

}


/* Class Operators */
TrieNodePointer& TrieNodePointer::operator=(const TrieNodePointer& ptr) {
    if (this != &ptr) {
        this->ptr.reset();
        this->ptr = std::unique_ptr<TrieClauses::Node>{new TrieClauses::Node{
            ptr.ptr->literal, ptr.ptr->children, ptr.ptr->size}};
    }

    return *this;
}
