#ifndef LITERALCOUNTS_HPP
#define LITERALCOUNTS_HPP

#include <memory>

#include "Bucket.hpp"


// This class represents the dynamic information about the number of literals
// in all the buckets.
class LiteralCounts {

    public:
        /* Class Constructors & Destructor */
        LiteralCounts();
        LiteralCounts(const int& numOfVariables);
        virtual ~LiteralCounts();


        /* Class Operators */
        friend std::ostream& operator<<(std::ostream& strm, const LiteralCounts& literalCounts);
        LiteralCounts& operator=(const LiteralCounts& counts);


        /* Getters & Setters */
        int getMin();
        int getMax();
        const int& getSize() const;


        /* Public Methods */
        // This method will add the number of literals of given tree
        void add(const TrieClauses& tree);

        // This method will add the number of literals of given bucket
        void add(const Bucket& bucket);

        // This method will remove the number of literals of given tree
        void remove(const TrieClauses& tree);

        // This method will remove the number of literals of given bucket
        void remove(const Bucket& bucket);


    private:
        /* Declaring Variables */
        std::unique_ptr<int[]> counts;
        int minBucket;
        int maxBucket;
        int size;

        /* Private Methods */
        // This method is a helper function to update recursively while traversing the tree
        void updateRecursive(const TrieClauses::Node* node, const bool& isAdd);

        // This method will update the min and max
        void updateMinMax(const int& index);
};


/* Inline Functions */
/* Getters & Setters */
inline const int& LiteralCounts::getSize() const {
    return size;
}

#endif // LITERALCOUNTS_HPP
