#ifndef CLAUSE_HPP
#define CLAUSE_HPP

#include <cassert>
#include <vector>

#include "Literal.hpp"

// This class represents a clause with multiple literals
class Clause {
    
    public:
        /* Class Constructors & Destructor */
        Clause();
        Clause(const std::vector<Literal>& clause);
        Clause(const int literals[], const unsigned& size, const int& key);
        Clause(Literal* literals, const unsigned& size, const int& key);
        virtual ~Clause();


        /* Class Operators */
        friend std::ostream& operator<<(std::ostream& strm, const Clause& clause);
        Clause& operator=(const Clause& clause);

        // Custom iterator for this Clause class
        typedef Literal* iterator;
        typedef const Literal* const_iterator;
        iterator begin();
        iterator end();
        const_iterator cbegin() const;
        const_iterator cend() const;


        /* Getters & Setters */
        const int& getKey() const;
        const unsigned& getSize() const;
        

        /* Public Methods */
        // This method will check whether the clause is sorted or not based on the orderings (for debugging)
        static bool isSorted(const Clause& clause);

        // This method will return the literal with given index
        const Literal& get(const unsigned& index) const;


    private:
        /* Declaring Variables */
        Literal* literals;
        unsigned size;
        int key;
};


/* Inline Functions */
// Custom iterator for this Clause class
inline Clause::iterator Clause::begin() {
    // handle when size is 0
    assert(size > 0 && "Clause size equals to 0");
    return &literals[0];
}


inline Clause::iterator Clause::end() {
    // handle when size is 0
    assert(size > 0 && "Clause size equals to 0");
    return &literals[size-1];
}


inline Clause::const_iterator Clause::cbegin() const {
    // handle when size is 0
    assert(size > 0 && "Clause size equals to 0");
    return &literals[0];
}


inline Clause::const_iterator Clause::cend() const {
    // handle when size is 0
    assert(size > 0 && "Clause size equals to 0");
    return &literals[size-1];
}


/* Getters & Setters */
inline const int& Clause::getKey() const {
    return key;
}


inline const unsigned& Clause::getSize() const {
    return size;
}

// This method will return the literal with given index
inline const Literal& Clause::get(const unsigned& index) const {
    return literals[index];
}

#endif // CLAUSE_HPP
