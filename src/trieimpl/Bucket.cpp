#include "Bucket.hpp"

/* Class Constructors & Destructor */
Bucket::Bucket() 
    : key{0}, posClauses{}, negClauses{} {

}


Bucket::Bucket(const int& key) 
    : key{key}, posClauses{key}, negClauses{-key} {

}


Bucket::Bucket(const Literal& literal) 
    : key{literal.getLiteral()},
        posClauses{literal.getLiteral()},
        negClauses{-literal.getLiteral()} {

}


Bucket::Bucket(const Bucket& bucket) {
    // copy the fields
    key = bucket.key;
    posClauses = bucket.posClauses;
    negClauses = bucket.negClauses;
}


Bucket::~Bucket(){

}


/* Class Operators */
std::ostream& operator<<(std::ostream& strm, const Bucket& bucket) {
    // DEBUG ONLY
    std::cout << "here" << std::endl;

    // handle empty bucket
    if (bucket.getSize() == 0) {
        return strm << "Bucket is empty" << std::endl;
    }

    // DEBUG ONLY
    std::cout << "here" << std::endl;

    strm << "Bucket " << bucket.key << " (size = " << bucket.getSize() << "):" << std::endl;

    // DEBUG ONLY
    std::cout << "here" << std::endl;

    strm << "posClause = " << bucket.posClauses << std::endl;
    strm << "negClause = " << bucket.negClauses << std::endl;

    return strm;
}


Bucket& Bucket::operator=(const Bucket& bucket) {
    if (this != &bucket) {
        // assign the fields for this bucket
        key = bucket.key;
        posClauses = bucket.posClauses;
        negClauses = bucket.negClauses;
    }

    return *this;
}


/* Public Methods */
void Bucket::add(const Clause& clause) {
    // check the clause key
    int clauseKey = clause.getKey();
    assert(((key == clauseKey) || (key == -clauseKey)) && "Adding clause to wrong bucket");

    // handle adding clause to the correct tree
    if (clauseKey > 0) {
        posClauses.add(clause);
    }
    else if (clauseKey < 0) {
        negClauses.add(clause);
    }
}


void Bucket::add(const TrieClauses& tree) {
    // check the tree key
    int treeKey = tree.getKey();
    assert(((key == treeKey) || (key == -treeKey)) && "Adding tree to wrong bucket");

    // handle adding tree to the correct tree
    if (treeKey > 0) {
        posClauses.add(tree);
    }
    else if (treeKey < 0) {
        negClauses.add(tree);
    }
}


TrieClauses Bucket::retrieve(const Literal& literal) {
    // retrieve the literal from both posClauses and negClauses
    TrieClauses result{};
    TrieClauses test = posClauses.retrieve(literal);
    result.add(test);
    result.add(negClauses.retrieve(literal));

    return result;
}


void Bucket::reset() {
    key = 0;
    posClauses.reset();
    negClauses.reset();
}
