#include "SequentialSolver.hpp"


/* Class Constructors & Destructor */
SequentialSolver::SequentialSolver() {

}


SequentialSolver::SequentialSolver(const std::string& input) 
    : inputFile{input}, staticOrdering{nullptr}, buckets{nullptr}, literalCounts{}, numOfVariables{-1} {

}


SequentialSolver::~SequentialSolver() {
    // delete all the data in buckets
    for (int i = 0; i < numOfVariables; ++i) {
        if (buckets[i] != nullptr) {
            delete buckets[i];
        }
    }
}


/* Public Methods */
void SequentialSolver::run() {
    // get the static orderings
    BOOST_LOG_TRIVIAL(warning) << "Computing Static Orderings";
    Timer timerStaticOrdering{"Computing Static Orderings"};
    computeStaticOrdering();
    BOOST_LOG_TRIVIAL(fatal) << timerStaticOrdering.result();
    
    // print the staic orderings
    for (int i = 0; i < numOfVariables; ++i) {
        BOOST_LOG_TRIVIAL(info) << "Var" << (i + 1) << " = " << staticOrdering[i];
    }

    // preprocess the data and get buckets
    BOOST_LOG_TRIVIAL(warning) << "Preprocessing the data";
    Timer timerPreprocess{"Preprocessing"};
    preprocess();
    BOOST_LOG_TRIVIAL(fatal) << timerPreprocess.result();

    // populate the literal counts
    Timer timerInitLiteralCounts{"Populate Literal Counts"};
    literalCounts = LiteralCounts{numOfVariables};
    for (int i = 0; i < numOfVariables; ++i) {
        literalCounts.add(*buckets[i]);
    }
    BOOST_LOG_TRIVIAL(fatal) << timerInitLiteralCounts.result();

    // print out the buckets
    printBuckets();

    // start with the first bucket
    BOOST_LOG_TRIVIAL(warning) << "Doing resolution with dynamic orderings";
    Bucket* current = buckets[0];
    Literal currentPos, currentNeg;
    bool isUnsatFound = false;
    int currentMin, currentIndex = 0;
    while (current != nullptr) {
        BOOST_LOG_TRIVIAL(info) << "Current Bucket = " << *current;

        // track the time
        Timer timerIteration{"Current Bucket"};

        // do resolution for the current bucket and delete it after finished
        Timer timerDoResolution{"Doing Resolution"};
        isUnsatFound = doResolution(current);
        delete current;
        buckets[currentIndex] = nullptr;
        BOOST_LOG_TRIVIAL(fatal) << timerDoResolution.result();
        BOOST_LOG_TRIVIAL(warning) << "Finished resolution for the current bucket + deleted the object";

        // handle UNSAT
        if (isUnsatFound) {
            BOOST_LOG_TRIVIAL(fatal) << "Final Result = UNSATISFIABLE";
            return;
        }

        // print out the new literalCounts info
        BOOST_LOG_TRIVIAL(info) << "New updated literalCounts = " << literalCounts;

        // DEBUG ONLY
        for (int i = 0; i < numOfVariables; ++i) {
            if (buckets[i] != nullptr) {
                std::cout << *buckets[i] << std::endl;
            }
        }

        // assign current to the min bucket
        currentMin = literalCounts.getMin();
        currentIndex = currentMin - 1;
        current = buckets[currentMin - 1];

        // DEBUG ONLY
        std::cout << "currentMin = " << currentMin << " " << ((current == nullptr) ? "null" : "not null") << std::endl;

        currentPos = Literal{currentMin};
        currentNeg = Literal{-currentMin};
        BOOST_LOG_TRIVIAL(info) << "New current bucket (before retrieval) = " << *current;

        // retrieve all the data about this bucket
        Timer timerRetrieveData{"Retrieving Data"};
        BOOST_LOG_TRIVIAL(warning) << "Retrieving data for the new current bucket";
        for (int i = 0; i < (currentMin - 1); ++i) {
            if (buckets[i] != nullptr) {
                // DEBUG ONLY
                std::cout << "i = " << i << std::endl;

                // retrieve and add the data
                current->add(buckets[i]->retrieve(currentPos));
                current->add(buckets[i]->retrieve(currentNeg));
            }
        }
        BOOST_LOG_TRIVIAL(fatal) << timerRetrieveData.result();
        BOOST_LOG_TRIVIAL(info) << "New current bucket (after retrieval) = " << *current;

        // get the timer
        BOOST_LOG_TRIVIAL(fatal) << timerIteration.result();
    }

    BOOST_LOG_TRIVIAL(fatal) << "Final Result = SATISFIABLE";
}


/* Private Methods */
void SequentialSolver::computeStaticOrdering() {
    // read the file
    std::ifstream infile{"../" + inputFile};
    std::string line;
    std::vector<std::pair<int,int>> counts;
    while (std::getline(infile, line)) {
        // don't handle lines starting with c
        if (line[0] == 'c') {
            continue;
        }

        // split the string
        std::vector<std::string> splitStr;
        boost::split(splitStr, line, boost::is_any_of("\t "));

        // get the total number of variables
        if (line[0] == 'p') {
            numOfVariables = std::stoi(splitStr[splitStr.size() - 2]);
            continue;
        }

        // populate the counts
        if ((counts.size() == 0) && (numOfVariables != -1)) {
            for (int i = 0; i < numOfVariables; ++i) {
                counts.push_back(std::make_pair(0, i+1));
            }
        }

        // loop through the splitStr
        for (unsigned i = 0; i < splitStr.size(); ++i) {
            if (std::stoi(splitStr[i]) == 0) {
                continue;
            }

            // increase the count of literals
            ++(counts[std::abs(std::stoi(splitStr[i])) - 1].first);
        }
    }

    // sort the counts 
    std::sort(counts.begin(), counts.end());

    // loop through counts and populate static ordering
    staticOrdering = std::unique_ptr<int[]>{new int[numOfVariables]};
    int index = 1;
    for (auto iter = counts.begin(); iter != counts.end(); ++iter) {
        // don't handle 0 (stay the same)
        if (iter->first == 0) {
            staticOrdering[iter->second - 1] = iter->second;
        } else {
            staticOrdering[iter->second - 1] = index++;
        }
    }
}


void SequentialSolver::preprocess() {
    // init the buckets
    buckets = new Bucket*[numOfVariables];
    for (int i = 0; i < numOfVariables; ++i) {
        buckets[i] = new Bucket{i+1};
    }
    
    // read the file
    std::ifstream infile{"../" + inputFile};
    std::string line;
    while (std::getline(infile, line)) {
        // don't handle 'c' and 'p'
        if ((line[0] == 'c') || (line[0] == 'p')) {
            continue;
        }

        // split the line
        std::vector<std::string> splitStr;
        boost::split(splitStr, line, boost::is_any_of("\t "));

        // compute the literals
        unsigned literalSize = splitStr.size() - 1;
        Literal* literals = new Literal[literalSize];
        int mapping, currNum;
        for (unsigned i = 0; i < literalSize; ++i) {
            // handle mapping
            currNum = std::stoi(splitStr[i]);
            mapping = staticOrdering[std::abs(currNum) - 1];
            literals[i] = (currNum < 0) ? Literal{-mapping} : Literal{mapping};
        }

        // sort the literals
        std::sort(literals, literals + literalSize);

        // instantiate the clause
        Clause clause{literals, literalSize, literals[0].getLiteral()};

        // add the clause to the right bucket
        buckets[std::abs(clause.getKey()) - 1]->add(clause);
    }
}


void SequentialSolver::printBuckets() {
    // print out original buckets
    for (int i = 0; i < numOfVariables; ++i) {
        if (buckets[i] == nullptr) {
            BOOST_LOG_TRIVIAL(info) << "Bucket " << (i + 1) << " is empty";
        } else {
            BOOST_LOG_TRIVIAL(info) << *buckets[i];
        }
    }
}


bool SequentialSolver::doResolution(Bucket* bucket) {
    // get the clauses from this bucket
    const TrieClauses& posClause = bucket->getPosClauses();
    const TrieClauses& negClause = bucket->getNegClauses();

    // base case (1 AND -1)
    if ((posClause.getSize() == 1) && (negClause.getSize() == 1) &&
        (posClause.get()->children.getSize() == 0) &&
        (negClause.get()->children.getSize() == 0)) {
        return true;
    }

    // loop through posClause recursively to get the clause and do resolution with negClause
    std::vector<Literal> literals{};
    doResolutionRecursive(posClause.get(), literals, negClause);

    // remove the literal counts for this bucket
    literalCounts.remove(*bucket);
    
    return false;
}


void SequentialSolver::doResolutionRecursive(const TrieClauses::Node* node, std::vector<Literal>& literals, const TrieClauses& resolutionWith) {
    // Base Case (when reach the leaves)
    if (node->children.getSize() == 0) {
        std::map<int,TrieClauses> resolution = resolutionWith.doResolution(Clause{literals});

        // update literalCounts with new resolution clauses
        // TODO: This method won't handle the counts after subsumption while
        // adding to the original tree 
        for (auto iter = resolution.cbegin(); iter != resolution.cend(); ++iter) {
            literalCounts.add(iter->second);
            buckets[std::abs(iter->first)-1]->add(iter->second);
        }

        return;
    }

    // loop through each child and keep going down
    for (auto iter = node->children.cbegin(); iter != node->children.cend(); ++iter) {
        literals.push_back((*iter).getLiteral());
        doResolutionRecursive((*iter).get(), literals, resolutionWith);
        literals.pop_back();
    }
}


/* Static Functions */
static void initLog() {
    // init boost log
    boost::log::add_common_attributes();
    boost::log::core::get()->add_global_attribute("Scope", boost::log::attributes::named_scope());

    // change boost log format
    // format: [TimeStamp] [ThreadId] [Severity Level] [Scope] log message
    auto boostTimeFormat = boost::log::expressions::format_date_time<boost::posix_time::ptime>("TimeStamp", "%H:%M:%S");
    auto boostSeverityFormat = boost::log::expressions::attr<boost::log::trivial::severity_level>("Severity");
    boost::log::formatter logFmt = boost::log::expressions::format("[%1%] [%2%] %3%")
        % boostTimeFormat % boostSeverityFormat % boost::log::expressions::smessage;

    // console sink
    auto consoleSink = boost::log::add_console_log(std::clog);
    consoleSink->set_formatter(logFmt);
}


/* Main Function */
int main(int argc, char* argv[]) {
    // Usage: ./sequentialsolver <input_file> <log_level (DEFAULT: INFO)>
    if (argc > 3 || argc < 2) {
        std::cout << "Usage: ./sequentialsolver <input_file> <log_level (DEFAULT: INFO)>" << std::endl;
        return 1;
    }

    // setup timer
    Timer timerTotalProgram{"Total Program"};  

    // handle the args
    Timer timerHandleArgs{"Handle Args"};
    std::string input{argv[1]};
    if (argc == 3) {
        // handle log level
        if (strcmp(argv[2], "WARN") == 0) {
            boost::log::core::get()->set_filter(boost::log::trivial::severity >= boost::log::trivial::warning);
        } else if (strcmp(argv[2], "ERROR") == 0) {
            boost::log::core::get()->set_filter(boost::log::trivial::severity >= boost::log::trivial::error);
        } else if (strcmp(argv[2], "OFF") == 0) {
            boost::log::core::get()->set_filter(boost::log::trivial::severity >= boost::log::trivial::fatal);
        } else {
            std::cout << "Wrong log_level input (use: WARN, ERROR, FATAL, OFF)" << std::endl;
            return 1;
        }
    } else {
        boost::log::core::get()->set_filter(boost::log::trivial::severity >= boost::log::trivial::info);
    }
    
    // init boost log
    initLog();
    BOOST_LOG_TRIVIAL(fatal) << timerHandleArgs.result();

    // run the SequentialSolver
    SequentialSolver solver{input};
    solver.run();

    // get timer
    BOOST_LOG_TRIVIAL(fatal) << timerTotalProgram.result();

    return 0;
}
