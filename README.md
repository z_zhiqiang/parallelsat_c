### Repo Structure
* **/inputs**: contain all the input text files (except sr15bench because of its size).
* **/lib**: contain all the libraries for this project
* **/src**: contain the source code.
* **/test**: contain test classes using google unit testing for c++
* **CMakeLists**: cmake file to generate make files and build
* **run.sh**: script to build and run the system

### How To Run
1. Install some necessary packages first: `sudo apt-get install make cmake libboost-all-dev`
3. Give the build script run permission: `chmod +x run.sh`
2. Run the build script: `./run.sh` (the script will create a `/build` folder and then run all the test cases + main file)
