#include "gtest/gtest.h"
#include "trieimpl/LiteralCounts.hpp"


TEST(literalcounts_test, test_add_with_tree) {
    // init test objects
    LiteralCounts test{6};
    TrieClauses test1{};
    int c1[4] {1,2,4,5};
    int c2[5] {1,3,4,5,6};
    int c3[3] {1,2,3};
    Clause cl1 = Clause(c1, 4, 1);
    Clause cl2 = Clause(c2, 5, 1);
    Clause cl3 = Clause(c3, 3, 1);
    test1.add(cl1);
    test1.add(cl2);
    test1.add(cl3);

    // test add
    test.add(test1);
    EXPECT_EQ(test.getMin(), 6);
    EXPECT_EQ(test.getMax(), 1);
    EXPECT_EQ(test.getSize(), 6);
}


TEST(literalcounts_test, test_add_with_bucket) {
    // init test objects
    LiteralCounts test{7};
    Bucket test1{1};
    int c1[5] {1,2,3,4,5}; 
    int c2[2] {1,2};
    int c3[4] {1,3,4,5};
    int c4[4] {1,3,4,7};
    int c5[4] {-1,3,5,7};
    int c6[2] {1,-3};
    int c7[4] {-1,-2,-3,-4};
    int c8[2] {-1,3};
    Clause cl1 = Clause{c1, 5, 1};
    Clause cl2 = Clause{c2, 2, 1};
    Clause cl3 = Clause{c3, 4, 1};
    Clause cl4 = Clause{c4, 4, 1};
    Clause cl5 = Clause{c5, 4, -1};
    Clause cl6 = Clause{c6, 2, 1};
    Clause cl7 = Clause{c7, 4, -1};
    Clause cl8 = Clause{c8, 2, -1};
    test1.add(cl1);
    test1.add(cl2);
    test1.add(cl3);
    test1.add(cl4);
    test1.add(cl5);
    test1.add(cl6);
    test1.add(cl7);
    test1.add(cl8);
 
    // test add
    test.add(test1);
    EXPECT_EQ(test.getMin(), 6);
    EXPECT_EQ(test.getMax(), 1);
    EXPECT_EQ(test.getSize(), 7);
}


TEST(literalcounts_test, test_remove_with_tree) {
    // init test objects
    LiteralCounts test{6};
    TrieClauses test1{}, test2{};
    int c1[4] {1,2,4,5};
    int c2[5] {1,3,4,5,6};
    int c3[3] {1,2,3};
    Clause cl1 = Clause(c1, 4, 1);
    Clause cl2 = Clause(c2, 5, 1);
    Clause cl3 = Clause(c3, 3, 1);
    test1.add(cl1);
    test1.add(cl2);
    test1.add(cl3);
    test2.add(cl1);
    test2.add(cl2);

    // test remove
    test.add(test1);
    test.remove(test1);
    EXPECT_EQ(test.getMin(), -1);
    EXPECT_EQ(test.getMax(), -1);
    EXPECT_EQ(test.getSize(), 6);
    test.add(test1);
    test.add(test2);
    test.remove(test2);
    EXPECT_EQ(test.getMin(), 6);
    EXPECT_EQ(test.getMax(), 1);
    EXPECT_EQ(test.getSize(), 6);
}


TEST(literalcounts_test, test_remove_with_bucket) {
    // init test objects
    LiteralCounts test{7};
    Bucket test1{1}, test2{1};
    int c1[5] {1,2,3,4,5}; 
    int c2[2] {1,2};
    int c3[4] {1,3,4,5};
    int c4[4] {1,3,4,7};
    int c5[4] {-1,3,5,7};
    int c6[2] {1,-3};
    int c7[4] {-1,-2,-3,-4};
    int c8[2] {-1,3};
    Clause cl1 = Clause{c1, 5, 1};
    Clause cl2 = Clause{c2, 2, 1};
    Clause cl3 = Clause{c3, 4, 1};
    Clause cl4 = Clause{c4, 4, 1};
    Clause cl5 = Clause{c5, 4, -1};
    Clause cl6 = Clause{c6, 2, 1};
    Clause cl7 = Clause{c7, 4, -1};
    Clause cl8 = Clause{c8, 2, -1};
    test1.add(cl1);
    test1.add(cl2);
    test1.add(cl3);
    test1.add(cl4);
    test1.add(cl5);
    test1.add(cl6);
    test1.add(cl7);
    test1.add(cl8);
    test2.add(cl1);
    test2.add(cl3);
    test2.add(cl7);
    test2.add(cl8);
 
    // test remove
    test.add(test1);
    test.remove(test1);
    EXPECT_EQ(test.getMin(), -1);
    EXPECT_EQ(test.getMax(), -1);
    EXPECT_EQ(test.getSize(), 7);
    test.add(test1);
    test.add(test2);
    test.remove(test2);
    EXPECT_EQ(test.getMin(), 6);
    EXPECT_EQ(test.getMax(), 1);
    EXPECT_EQ(test.getSize(), 7);
}

