#include "gtest/gtest.h"
#include "trieimpl/TrieClauses.hpp"

#include <iostream>

TEST(trieclauses_test, test_copyconstructor) {
    // init test objects
    TrieClauses test1{};
    int c1[5] {1,2,3,4,5};
    int c2[5] {1,3,4,5,6};
    int c3[2] {1,2};
    Clause cl1 = Clause(c1, 5, 1);
    Clause cl2 = Clause(c2, 5, 1);
    Clause cl3 = Clause(c3, 2, 1);
    test1.add(cl1);
    TrieClauses test2{test1};

    // test copy constructor
    EXPECT_EQ(test1.getSize(), 1);
    EXPECT_EQ(test2.getSize(), 1);
    test1.add(cl2);
    EXPECT_EQ(test1.getSize(), 2);
    EXPECT_EQ(test2.getSize(), 1);
    test2.add(cl3);
    EXPECT_EQ(test1.getSize(), 2);
    EXPECT_EQ(test2.getSize(), 1);
}


TEST(trieclauses_test, test_assign_operator) {
    // init test objects
    TrieClauses test1{};
    int c1[5] {1,2,3,4,5};
    int c2[5] {1,3,4,5,6};
    int c3[2] {1,2};
    Clause cl1 = Clause(c1, 5, 1);
    Clause cl2 = Clause(c2, 5, 1);
    Clause cl3 = Clause(c3, 2, 1);
    test1.add(cl1);
    TrieClauses test2 = test1;

    // test assign operator 
    EXPECT_EQ(test1.getSize(), 1);
    EXPECT_EQ(test2.getSize(), 1);
    test1.add(cl2);
    EXPECT_EQ(test1.getSize(), 2);
    EXPECT_EQ(test2.getSize(), 1);
    test2.add(cl3);
    EXPECT_EQ(test1.getSize(), 2);
    EXPECT_EQ(test2.getSize(), 1); 
}


TEST(trieclauses_test, test_add_with_clause) {
    // init test objects
    TrieClauses test{};
    int c1[5] {1,2,3,4,5}; 
    int c2[2] {1,2};
    int c3[4] {1,3,4,5};
    int c4[4] {1,3,4,7};
    int c5[4] {1,3,5,7};
    int c6[2] {1,-3};
    int c7[4] {1,-2,-3,-4};
    int c8[2] {1,3};
    Clause cl1 = Clause{c1, 5, 1};
    Clause cl2 = Clause{c2, 2, 1};
    Clause cl3 = Clause{c3, 4, 1};
    Clause cl4 = Clause{c4, 4, 1};
    Clause cl5 = Clause{c5, 4, 1};
    Clause cl6 = Clause{c6, 2, 1};
    Clause cl7 = Clause{c7, 4, 1};
    Clause cl8 = Clause{c8, 2, 1};

    // test add
    test.add(cl1);
    EXPECT_EQ(test.getSize(), 1);
    test.add(cl1);
    EXPECT_EQ(test.getSize(), 1);
    test.add(cl2);
    EXPECT_EQ(test.getSize(), 1);
    test.add(cl1);
    EXPECT_EQ(test.getSize(), 1);
    test.add(cl3);
    EXPECT_EQ(test.getSize(), 2);
    test.add(cl1);
    test.add(cl2);
    test.add(cl3);
    EXPECT_EQ(test.getSize(), 2);
    test.add(cl4);
    test.add(cl5);
    test.add(cl6);
    test.add(cl7);
    EXPECT_EQ(test.getSize(), 6);
    test.add(cl8);
    EXPECT_EQ(test.getSize(), 4);
}


TEST(trieclauses_test, test_add_with_trie) {
    // init test objects
    TrieClauses test1{}, test2{}, test3{};
    int c1[5] {1,2,3,4,5}; 
    int c2[2] {1,2};
    int c3[4] {1,3,4,5};
    int c4[4] {1,3,4,7};
    int c5[4] {1,3,5,7};
    int c6[2] {1,-3};
    int c7[4] {1,-2,-3,-4};
    int c8[2] {1,3};
    Clause cl1 = Clause{c1, 5, 1};
    Clause cl2 = Clause{c2, 2, 1};
    Clause cl3 = Clause{c3, 4, 1};
    Clause cl4 = Clause{c4, 4, 1};
    Clause cl5 = Clause{c5, 4, 1};
    Clause cl6 = Clause{c6, 2, 1};
    Clause cl7 = Clause{c7, 4, 1};
    Clause cl8 = Clause{c8, 2, 1};

    // test add
    test1.add(cl1);
    test1.add(cl3);
    test1.add(cl4);
    test1.add(cl5);
    EXPECT_EQ(test1.getSize(), 4);
    test2.add(test1);
    EXPECT_EQ(test1.getSize(), test2.getSize());
    EXPECT_EQ(test1.getKey(), test2.getKey());
    test1.add(cl2);
    test2.add(test1);
    test2.add(test2);
    EXPECT_EQ(test1.getSize(), test2.getSize());
    EXPECT_EQ(test1.getKey(), test2.getKey());
    test2.add(cl6);
    test2.add(cl7);
    EXPECT_EQ(test2.getSize(), 6);
    test3.add(cl6);
    test3.add(cl8);
    EXPECT_EQ(test3.getSize(), 2);
    EXPECT_EQ(test1.getSize(), 4);
    test1.add(test3);
    test2.add(test3);
    EXPECT_EQ(test1.getSize(), 3);
    EXPECT_EQ(test2.getSize(), 4);
}


TEST(trieclauses_test, test_retrieve) {
    // init test objects
    TrieClauses test1{}, test2{};
    int c1[6] {1,2,3,4,5,9};
    int c2[6] {1,2,3,4,5,7};
    int c3[5] {1,2,3,4,7};
    int c4[5] {1,2,3,5,10};
    int c5[5] {1,2,3,7,10};
    int c6[4] {1,2,5,7};
    int c7[4] {1,2,5,8};
    int c8[4] {1,2,6,9};
    int c9[4] {1,2,6,10};
    int c10[4] {1,3,5,9};
    int c11[4] {1,3,5,10};
    int c12[2] {1,4};
    int c13[3] {1,5,10};
    int c14[3] {1,5,11};
    int c15[3] {1,6,10};
    int c16[3] {1,6,11};
    Clause cl1 = Clause{c1, 6, 1};
    Clause cl2 = Clause{c2, 6, 1};
    Clause cl3 = Clause{c3, 5, 1};
    Clause cl4 = Clause{c4, 5, 1};
    Clause cl5 = Clause{c5, 5, 1};
    Clause cl6 = Clause{c6, 4, 1};
    Clause cl7 = Clause{c7, 4, 1};
    Clause cl8 = Clause{c8, 4, 1};
    Clause cl9 = Clause{c9, 4, 1};
    Clause cl10 = Clause{c10, 4, 1};
    Clause cl11 = Clause{c11, 4, 1};
    Clause cl12 = Clause{c12, 2, 1};
    Clause cl13 = Clause{c13, 3, 1};
    Clause cl14 = Clause{c14, 3, 1};
    Clause cl15 = Clause{c15, 3, 1};
    Clause cl16 = Clause{c16, 3, 1};
    test1.add(cl1);
    test1.add(cl2);
    test1.add(cl3);
    test1.add(cl4);
    test1.add(cl5);
    test1.add(cl6);
    test1.add(cl7);
    test1.add(cl8);
    test1.add(cl9);
    test1.add(cl10);
    test1.add(cl11);
    test1.add(cl12);
    test1.add(cl13);
    test1.add(cl14);
    test1.add(cl15);
    test1.add(cl16);

    // test retrieve
    EXPECT_EQ(test1.getSize(), 16);
    test2 = test1.retrieve(Literal{5});
    EXPECT_EQ(test1.getSize(), 7);
    EXPECT_EQ(test2.getSize(), 9);
}


TEST(trieclauses_test, test_do_resolution) {
    // init test objects
    TrieClauses test1{};
    int c1[6] {1,2,3,4,5,9};
    int c2[6] {1,2,3,4,5,7};
    int c3[5] {1,2,3,4,7};
    int c4[5] {1,2,3,5,10};
    int c5[5] {1,2,3,7,10};
    int c6[4] {1,2,5,7};
    int c7[4] {1,2,5,8};
    int c8[4] {1,2,6,9};
    int c9[4] {1,2,6,10};
    int c10[4] {1,3,5,9};
    int c11[4] {1,3,5,10};
    int c12[2] {1,4};
    int c13[3] {1,5,10};
    int c14[3] {1,5,11};
    int c15[3] {1,6,10};
    int c16[3] {1,6,11};
    Clause cl1 = Clause{c1, 6, 1};
    Clause cl2 = Clause{c2, 6, 1};
    Clause cl3 = Clause{c3, 5, 1};
    Clause cl4 = Clause{c4, 5, 1};
    Clause cl5 = Clause{c5, 5, 1};
    Clause cl6 = Clause{c6, 4, 1};
    Clause cl7 = Clause{c7, 4, 1};
    Clause cl8 = Clause{c8, 4, 1};
    Clause cl9 = Clause{c9, 4, 1};
    Clause cl10 = Clause{c10, 4, 1};
    Clause cl11 = Clause{c11, 4, 1};
    Clause cl12 = Clause{c12, 2, 1};
    Clause cl13 = Clause{c13, 3, 1};
    Clause cl14 = Clause{c14, 3, 1};
    Clause cl15 = Clause{c15, 3, 1};
    Clause cl16 = Clause{c16, 3, 1};
    test1.add(cl1);
    test1.add(cl2);
    test1.add(cl3);
    test1.add(cl4);
    test1.add(cl5);
    test1.add(cl6);
    test1.add(cl7);
    test1.add(cl8);
    test1.add(cl9);
    test1.add(cl10);
    test1.add(cl11);
    test1.add(cl12);
    test1.add(cl13);
    test1.add(cl14);
    test1.add(cl15);
    test1.add(cl16);

    // test resolution
    int cResolution[2] {-1,-5};
    Clause clResolution = Clause{cResolution, 2, -1};
    std::map<int,TrieClauses> resolution = test1.doResolution(clResolution);
    for (auto iter = resolution.cbegin(); iter != resolution.cend(); ++iter) {
        std::cout << "key = " << iter->first << ", value = " << iter->second << std::endl;
    }
}
