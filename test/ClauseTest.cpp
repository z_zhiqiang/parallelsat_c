#include "gtest/gtest.h"
#include "trieimpl/Clause.hpp"


TEST(clause_test, test_iterator) {
    // init test objects
    Literal* literals;
    literals = new Literal[10];
    for (int i = 0; i < 10; i++) {
        literals[i] = Literal(i);
    }
    Clause test = Clause(literals, 10, 1);

    // test the iterators
    Clause::iterator iter;
    Clause::const_iterator iterConst;
    int i = 0;
    for (iter = test.begin(); iter != test.end(); iter++) {
        EXPECT_EQ(*iter, i++);
    }
    EXPECT_EQ(i, 9);
    i = 0;
    for (iterConst = test.cbegin(); iterConst != test.cend(); iterConst++) {
        EXPECT_EQ(*iterConst, i++);
    }
    EXPECT_EQ(i, 9);
}


TEST(clause_test, test_getKey) {
    // init test objects
    Literal* literals;
    literals = new Literal[10];
    for (int i = 0; i < 10; i++) {
        literals[i] = Literal(i);
    }
    Clause test = Clause(literals, 10, 1);

    // test the key
    EXPECT_EQ(test.getKey(), 1);
}


TEST(clause_test, test_isSorted) {
    // init test objects
    Literal *literals1, *literals2, *literals3, *literals4, *literals5, *literals6, *literals7;

    literals1 = new Literal[10];
    for (int i = 0; i < 10; i++) {
        literals1[i] = Literal(i);
    }
    Clause test1 = Clause(literals1, 10, 1);

    literals2 = new Literal[10]; 
    for (int i = 0; i < 10; i++) {
        literals2[i] = Literal(-i);
    }
    Clause test2 = Clause(literals2, 10, -1);

    literals3 = new Literal[10]; 
    for (int i = 0; i < 10; i++) {
        literals3[i] = (i % 2 == 0) ? Literal(i) : Literal(-i);
    }
    Clause test3 = Clause(literals3, 10, -1);

    literals4 = new Literal[10]; 
    for (int i = 0; i < 9; i++) {
        literals4[i] = Literal(-1);
    }
    literals4[9] = Literal(1);
    Clause test4 = Clause(literals4, 10, -1);

    literals5 = new Literal[10]; 
    for (int i = 0; i < 10; i++) {
        literals5[i] = Literal(i);
    }
    literals5[2] = Literal(-1);
    Clause test5 = Clause(literals5, 10, -1);

    literals6 = new Literal[10]; 
    for (int i = 10; i > 0; i--) {
        literals6[10-i] = Literal(i);
    }
    Clause test6 = Clause(literals6, 10, -1);

    literals7 = new Literal[10]; 
    for (int i = 10; i > 0; i--) {
        literals7[10-i] = Literal(-i);
    }
    Clause test7 = Clause(literals7, 10, -1);

    // test isSorted
    EXPECT_TRUE(Clause::isSorted(test1));
    EXPECT_TRUE(Clause::isSorted(test2));
    EXPECT_TRUE(Clause::isSorted(test3));
    EXPECT_TRUE(Clause::isSorted(test4));
    EXPECT_FALSE(Clause::isSorted(test5));
    EXPECT_FALSE(Clause::isSorted(test6));
    EXPECT_FALSE(Clause::isSorted(test7));
}


TEST(clause_test, test_get) {
    // init test objects
    Literal* literals;
    literals = new Literal[10];
    for (int i = 0; i < 10; i++) {
        literals[i] = Literal(i);
    }
    Clause test = Clause(literals, 10, 1);

    // test get
    for (int i = 0; i < 10; i++) {
        EXPECT_EQ(test.get(i), Literal(i));
    }
}


TEST(clause_test, test_assign_operator) {
    // init test objects
    Literal* literals1;
    Literal* literals2;
    literals1 = new Literal[10];
    literals2 = new Literal[10];
    for (int i = 0; i < 10; i++) {
        literals1[i] = Literal(i);
        literals2[i] = Literal(-i);
    }
    Clause test1 = Clause(literals1, 10, 1);
    Clause test2 = Clause(literals2, 10, -1);

    // test the assignment operator
    test1 = test2;
    for (int i = 0; i < 10; i++) {
        EXPECT_EQ(test1.get(i), Literal(-i));
    }
    EXPECT_EQ(test1.getKey(), -1);
}
