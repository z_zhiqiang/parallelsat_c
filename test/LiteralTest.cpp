#include "gtest/gtest.h"
#include "trieimpl/Literal.hpp"


TEST(literal_test, test_equal) {
    Literal test1 = Literal(10);
    Literal test2 = Literal(10);
    Literal test3 = Literal(-10);
    Literal test4 = Literal(-1);

    EXPECT_EQ(test1, test2);
    EXPECT_NE(test1, test3);
    EXPECT_NE(test1, test4);
    EXPECT_NE(test2, test3);
    EXPECT_NE(test2, test4); 
}


TEST(literal_test, test_less) {
    Literal test1 = Literal(10);
    Literal test2 = Literal(10);
    Literal test3 = Literal(-10);
    Literal test4 = Literal(-1);

    EXPECT_LT(test3, test1);
    EXPECT_LT(test4, test2);
    EXPECT_LT(test4, test1);
    EXPECT_LT(test4, test3);
}


TEST(literal_test, test_greater) {
    Literal test1 = Literal(10);
    Literal test2 = Literal(10);
    Literal test3 = Literal(-10);
    Literal test4 = Literal(-1);

    EXPECT_GT(test1, test4);
    EXPECT_GT(test2, test3);
}


TEST(literal_test, test_isOpposite) {
    Literal test1 = Literal(10);
    Literal test2 = Literal(10);
    Literal test3 = Literal(-10);
    Literal test4 = Literal(-1);

    EXPECT_TRUE(Literal::isOpposite(test1, test3));
    EXPECT_TRUE(Literal::isOpposite(test2, test3));
    EXPECT_FALSE(Literal::isOpposite(test1, test4));
    EXPECT_FALSE(Literal::isOpposite(test3, test4));
}


TEST(literal_test, test_assign_operator) {
    Literal test1 = Literal(1);
    Literal test2 = Literal(-1);

    EXPECT_NE(test1, test2);
    test1 = test2;
    EXPECT_EQ(test1, test2);
}
