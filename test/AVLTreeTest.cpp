#include <iostream>

#include "gtest/gtest.h"
#include "trieimpl/AVLTree.hpp"


TEST(avltree_test, test_assign_operator) {
    // init test objects
    AVLTree<int> test1{}, test2{};
    for (int i = 10; i > 0; i--) {
        test2.add(i);
    }
   
    // test the assignment operator
    EXPECT_EQ(test2.getSize(), 10);
    EXPECT_EQ(test1.getSize(), 0);
    test1 = test2;
    EXPECT_EQ(test1.getSize(), 10);
    for (int i = 10; i > 0; i--) {
        EXPECT_TRUE(test1.contains(i));
    }
}


TEST(avltree_test, test_iterator) {
    // init test objects
    AVLTree<int> test{};
    for (int i = 10; i > 0; i--) {
        test.add(i);
        test.add(-i);
    }
    test.add(0);

    // test the iterator
    int i = -10;
    for (AVLTree<int>::iterator iter = test.begin(); iter != test.end(); iter++) {
        EXPECT_EQ(*iter, i++);
        *iter = 11;
        EXPECT_EQ(*iter, 11);
        *iter = i - 1;
        EXPECT_EQ(test.getSize(), 21);
    }
    EXPECT_EQ(i, 11);

    // test the const_iterator
    i = -10;
    for (AVLTree<int>::const_iterator iter = test.cbegin(); iter != test.cend(); iter++) {
        EXPECT_EQ(*iter, i++);
        EXPECT_EQ(test.getSize(), 21);
    }
    EXPECT_EQ(i, 11);
}


TEST(avltree_test, test_add) {
    // init test objects
    AVLTree<int> test{};
    for (int i = 10; i > 0; i--) {
        test.add(i);
        test.add(-i);
    }

    // test the add function
    EXPECT_EQ(test.getSize(), 20);
    int& a = test.add(100);
    EXPECT_EQ(test.getSize(), 21);
    EXPECT_TRUE(test.contains(100));
    a = 200;
    EXPECT_EQ(test.getSize(), 21);
    EXPECT_TRUE(test.contains(200));
    EXPECT_FALSE(test.contains(100));
}


TEST(avltree_test, test_remove) {
    // init test objects
    AVLTree<int> test{};
    for (int i = 10; i > 0; i--) {
        test.add(i);
        test.add(-i);
    }

    // test the remove function
    EXPECT_EQ(test.getSize(), 20);
    for (int i = 10; i > 0; i--) {
        test.remove(i);
    }
    EXPECT_EQ(test.getSize(), 10);
    for (int i = 10; i > 0; i--) {
        test.remove(-i);
    }
    EXPECT_EQ(test.getSize(), 0);
}


TEST(avltree_test, test_reset) {
    // init test objects
    AVLTree<int> test{};
    for (int i = 10; i > 0; i--) {
        test.add(i);
        test.add(-i);
    }
    
    // test reset
    EXPECT_EQ(test.getSize(), 20);
    EXPECT_TRUE(test.contains(10));
    test.reset();
    EXPECT_EQ(test.getSize(), 0);
    EXPECT_FALSE(test.contains(10));
    test.add(10);
    EXPECT_EQ(test.getSize(), 1);
    EXPECT_TRUE(test.contains(10));
}


TEST(avltree_test, test_contains) {
    // init test objects
    AVLTree<int> test{};
    for (int i = 10; i > 0; i--) {
        test.add(i);
        test.add(-i);
    }

    // test the contains function
    for (int i = 10; i > 0; i--) {
        EXPECT_TRUE(test.contains(i));
        EXPECT_TRUE(test.contains(-i));
    }
    EXPECT_FALSE(test.contains(0));
    EXPECT_EQ(test.getSize(), 20);
}


TEST(avltree_test, test_find) {
    // init test objects
    AVLTree<int> test{};
    for (int i = 10; i > 0; i--) {
        test.add(i);
        test.add(-i);
    }

    // test the find function
    for (int i = 10; i > 0; i--) {
        EXPECT_EQ(test.find(i), i);
        EXPECT_EQ(test.find(-i), -i);
        EXPECT_EQ(test.getSize(), 20);
    }
    EXPECT_FALSE(test.contains(100));
    EXPECT_TRUE(test.contains(10));
    test.find(10) = 100;
    EXPECT_TRUE(test.contains(100));
    EXPECT_FALSE(test.contains(10));
    int& ptr = test.find(100);
    ptr = 10;
    EXPECT_FALSE(test.contains(100));
    EXPECT_TRUE(test.contains(10));
}


TEST(avltree_test, test_pop) {
    // init test objects
    AVLTree<int> test{};
    for (int i = 10; i > 0; i--) {
        test.add(i);
        test.add(-i);
    }

    // test the pop function
    for (int i = 10; i > 0; i--) {
        EXPECT_EQ(test.pop(i), i);
        EXPECT_EQ(test.pop(-i), -i);
        EXPECT_EQ(test.getSize(), 20 - (11 - i) * 2);
    }
}
