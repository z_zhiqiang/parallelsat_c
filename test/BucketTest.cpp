#include "gtest/gtest.h"
#include "trieimpl/Bucket.hpp"


TEST(bucket_test, test_add_with_clause) {
    // init test objects
    Bucket test1{1};
    int c1[5] {1,2,3,4,5}; 
    int c2[2] {1,2};
    int c3[4] {1,3,4,5};
    int c4[4] {1,3,4,7};
    int c5[4] {-1,3,5,7};
    int c6[2] {1,-3};
    int c7[4] {-1,-2,-3,-4};
    int c8[2] {-1,3};
    Clause cl1 = Clause{c1, 5, 1};
    Clause cl2 = Clause{c2, 2, 1};
    Clause cl3 = Clause{c3, 4, 1};
    Clause cl4 = Clause{c4, 4, 1};
    Clause cl5 = Clause{c5, 4, -1};
    Clause cl6 = Clause{c6, 2, 1};
    Clause cl7 = Clause{c7, 4, -1};
    Clause cl8 = Clause{c8, 2, -1};
 
    // test add
    test1.add(cl1);
    test1.add(cl2);
    test1.add(cl3);
    test1.add(cl4);
    test1.add(cl5);
    test1.add(cl6);
    test1.add(cl7);
    test1.add(cl8);
    EXPECT_EQ(test1.getPosClauses().getSize(), 4);
    EXPECT_EQ(test1.getNegClauses().getSize(), 2);
    EXPECT_EQ(test1.getSize(), 6);
}


TEST(bucket_test, test_add_with_tree) {
    // init test objects
    TrieClauses test1{}, test2{};
    Bucket test{1};
    int c1[5] {1,2,3,4,5}; 
    int c2[2] {1,2};
    int c3[4] {1,3,4,5};
    int c4[4] {1,3,4,7};
    int c5[4] {1,3,5,7};
    int c6[2] {-1,-3};
    int c7[4] {-1,-2,-3,-4};
    int c8[2] {-1,3};
    Clause cl1 = Clause{c1, 5, 1};
    Clause cl2 = Clause{c2, 2, 1};
    Clause cl3 = Clause{c3, 4, 1};
    Clause cl4 = Clause{c4, 4, 1};
    Clause cl5 = Clause{c5, 4, 1};
    Clause cl6 = Clause{c6, 2, -1};
    Clause cl7 = Clause{c7, 4, -1};
    Clause cl8 = Clause{c8, 2, -1};
    test1.add(cl1);
    test1.add(cl2);
    test1.add(cl3);
    test1.add(cl4);
    test1.add(cl5);
    test2.add(cl6);
    test2.add(cl7);
    test2.add(cl8);

    // test add
    test.add(test1);
    test.add(test2);
    EXPECT_EQ(test.getPosClauses().getSize(), 4);
    EXPECT_EQ(test.getNegClauses().getSize(), 3);
    EXPECT_EQ(test.getSize(), 7);
}


TEST(bucket_test, test_retrieve) {
    // init test objects
    Bucket test1{1};
    int c1[5] {1,2,3,4,5}; 
    int c2[2] {1,2};
    int c3[4] {1,3,4,5};
    int c4[4] {1,3,4,7};
    int c5[4] {-1,3,5,7};
    int c6[2] {1,-3};
    int c7[4] {-1,-2,-3,-4};
    int c8[2] {-1,3};
    Clause cl1 = Clause{c1, 5, 1};
    Clause cl2 = Clause{c2, 2, 1};
    Clause cl3 = Clause{c3, 4, 1};
    Clause cl4 = Clause{c4, 4, 1};
    Clause cl5 = Clause{c5, 4, -1};
    Clause cl6 = Clause{c6, 2, 1};
    Clause cl7 = Clause{c7, 4, -1};
    Clause cl8 = Clause{c8, 2, -1};
    test1.add(cl1);
    test1.add(cl2);
    test1.add(cl3);
    test1.add(cl4);
    test1.add(cl5);
    test1.add(cl6);
    test1.add(cl7);
    test1.add(cl8);
    
    // test retrieve
    EXPECT_EQ(test1.getPosClauses().getSize(), 4);
    EXPECT_EQ(test1.getNegClauses().getSize(), 2);
    EXPECT_EQ(test1.getSize(), 6);
    TrieClauses test = test1.retrieve(Literal{3});
    EXPECT_EQ(test.getSize(), 3);
    EXPECT_EQ(test1.getPosClauses().getSize(), 2);
    EXPECT_EQ(test1.getNegClauses().getSize(), 1);
    EXPECT_EQ(test1.getSize(), 3);
}
